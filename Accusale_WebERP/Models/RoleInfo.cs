﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AccuSale_Final.Models
{
    public class RoleInfo
    {
        public decimal RoleId { get; set; }     
        
        [Required(ErrorMessage = "Enter Role")]
        public string Role { get; set; }


        public string Narration { get; set; }

        public DateTime? extraDate { get; set; }   
        public string Extra1 { get; set; }= string.Empty;
        public string Extra2 { get; set; } = string.Empty;


    }
}