﻿using Accusale.WindowsApp;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class ServiceCategoryController : Controller
    {
        ServiceCategorySP spServiceCategory = new ServiceCategorySP();
        // GET: ServiceCategory
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddServiceCategory(ServiceCategoryInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.ServicecategoryId == 0)
            {
                if (spServiceCategory.ServiceCategoryCheckIfExist(datainfo.CategoryName, 0) == false)
                {
                    spServiceCategory.ServiceCategoryAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.ServicecategoryId != 0)
            {
                spServiceCategory.ServiceCategoryEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteServiceCategory(int ServicecategoryId)
        {
            spServiceCategory.ServiceCategoryDelete(ServicecategoryId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spServiceCategory.ServiceCategoryViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int ServicecategoryId)
        {
            ServiceCategoryInfo datainfo = spServiceCategory.ServiceCategoryView(ServicecategoryId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}