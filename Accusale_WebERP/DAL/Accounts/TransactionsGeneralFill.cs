﻿
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;
namespace AccuSale_Final.DAL
{
    class TransactionsGeneralFill 
    {
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        /// <summary>
        /// Function to fill all cash or bank ledgers for String
        /// </summary>
        /// <param name="cmbCashOrBank"></param>
        /// <param name="isAll"></param>
        public DataTable CashOrBankComboFill()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
            SqlDataAdapter sdaadapter = new SqlDataAdapter("CashOrBankComboFill", Sqlcon);
            sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        //public void CashOrBankComboFill(Combo , bool isAll)
        //{
        //    DataTable dtbl = new DataTable();
        //    SqlDataAdapter sdaadapter = new SqlDataAdapter("CashOrBankComboFill", Sqlcon);
        //    try
        //    {
        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        sdaadapter.Fill(dtbl);
        //        cmbCashOrBank.DataSource = dtbl;
        //        cmbCashOrBank.ValueMember = "ledgerId";
        //        cmbCashOrBank.DisplayMember = "ledgerName";
        //        cmbCashOrBank.SelectedIndex = -1;
        //    }
        //    catch (Exception ex)
        //    {
        //         ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //}
        /// <summary>
        /// Function to fill all cash or parties ledgers to String
        /// </summary>
        /// <param name="cmbCashOrParty"></param>
        /// <param name="isAll"></param>
        //public void CashOrPartyComboFill(String cmbCashOrParty, bool isAll)
        //{
        //    // Fill all cash or party under Sundry Crediter
        //    DataTable dtblCashOrParty = new DataTable();
        //    try
        //    {
        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        SqlDataAdapter sqlda = new SqlDataAdapter("CashOrPartyComboFill", Sqlcon);
        //        sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        sqlda.Fill(dtblCashOrParty);
        //        cmbCashOrParty.SelectedIndex = -1;
        //        if (isAll)
        //        {
        //            DataRow dr = dtblCashOrParty.NewRow();
        //            dr["ledgerName"] = "All";
        //            dr["ledgerId"] = 0;
        //            dtblCashOrParty.Rows.InsertAt(dr, 0);
        //        }
        //        cmbCashOrParty.DataSource = dtblCashOrParty;
        //        cmbCashOrParty.DisplayMember = "ledgerName";
        //        cmbCashOrParty.ValueMember = "ledgerId";
        //    }
        //    catch (Exception ex)
        //    {
        //         ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //}
        /// <summary>
        /// Function to get next VoucherNo for voucher on Automatic generation 
        /// </summary>
        /// <param name="VoucherTypeId"></param>
        /// <param name="txtBox"></param>
        /// <param name="date"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public string VoucherNumberAutomaicGeneration(decimal VoucherTypeId, decimal txtBox, DateTime date, string tableName)
        {
            string strVoucherNo = string.Empty;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("VoucherNumberAutomaicGeneration", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = VoucherTypeId;
                sprmparam = sccmd.Parameters.Add("@txtBox", SqlDbType.Decimal);
                sprmparam.Value = txtBox;
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = date;
                sprmparam = sccmd.Parameters.Add("@tab_name", SqlDbType.VarChar);
                sprmparam.Value = tableName;
                object obj = sccmd.ExecuteScalar();
                if (obj != null)
                {
                    strVoucherNo = obj.ToString();
                }
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return strVoucherNo;
        }
        //public DataTable VoucherTypeComboFill(String cmbVoucherType, string strVoucherType, bool isAll)
        //{
        //    DataTable dtblVoucherType = new DataTable();
        //    try
        //    {

        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        SqlDataAdapter sdaadapter = new SqlDataAdapter("VoucherTypeSelectionComboFill", Sqlcon);
        //        sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        sdaadapter.SelectCommand.Parameters.Add("@strVoucherType", SqlDbType.VarChar).Value = strVoucherType;
        //        sdaadapter.Fill(dtblVoucherType);
        //        cmbVoucherType.SelectedIndex = -1;
        //        if (isAll)
        //        {
        //            DataRow dRow = dtblVoucherType.NewRow();
        //            dRow["voucherTypeId"] = 0;
        //            dRow["voucherTypeName"] = "All";
        //            dtblVoucherType.Rows.InsertAt(dRow, 0);
        //        }
        //        cmbVoucherType.DataSource = dtblVoucherType;
        //        cmbVoucherType.DisplayMember = "voucherTypeName";
        //        cmbVoucherType.ValueMember = "voucherTypeId";
        //    }
        //    catch (Exception ex)
        //    {

        //        MessageBox.Show("TGF:" + ex.Message, StrResources.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
        //    return dtblVoucherType;

        //}
        /// <summary>
        /// Function to fill all Currencies created for combobx
        /// </summary>
        /// <returns></returns>
        public DataTable CurrencyComboFill()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("CurrencyComboFill", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all AccountLedgers under Expenses forcombox
        /// </summary>
        /// <param name="cmbType"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        //public DataTable AccountLedgerUnderExpenses(String cmbType, bool isAll)
        //{
        //    DataTable dtbl = new DataTable();
        //    try
        //    {
        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        SqlDataAdapter sdaadapter = new SqlDataAdapter("AccountLedgerUnderExpenses", Sqlcon);
        //        sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        sdaadapter.Fill(dtbl);
        //        if (isAll)
        //        {
        //            DataRow dr = dtbl.NewRow();
        //            dr["ledgerName"] = "All";
        //            dr["ledgerId"] = 0;
        //            dtbl.Rows.InsertAt(dr, 0);
        //        }
        //        cmbType.DataSource = dtbl;
        //        cmbType.DisplayMember = "ledgerName";
        //        cmbType.ValueMember = "ledgerId";
        //    }
        //    catch (Exception ex)
        //    {
        //         ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //    return dtbl;
        //}
        /// <summary>
        /// Function to fill all the units corresponding to product in String in Datagridview
        /// </summary>
        /// <param name="dgvCurrent"></param>
        /// <param name="strProductId"></param>
        /// <param name="inRowIndex"></param>
        /// <returns></returns>
        //public DataTable UnitViewAllByProductId(DataGridView dgvCurrent, string strProductId, int inRowIndex)
        //{
        //    DataTable dtblUnitViewAll = new DataTable();
        //    try
        //    {
        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        SqlDataAdapter sdaadapter = new SqlDataAdapter("UnitViewAllByProductId", Sqlcon);
        //        sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        SqlParameter sqlparameter = new SqlParameter();
        //        sqlparameter = sdaadapter.SelectCommand.Parameters.Add("@productId", SqlDbType.VarChar);
        //        sqlparameter.Value = strProductId;
        //        sdaadapter.Fill(dtblUnitViewAll);
        //    }
        //    catch (Exception ex)
        //    {
        //         ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //    try
        //    {
        //        DataGridViewStringCell dgvcmbUnit = (DataGridViewStringCell)dgvCurrent[dgvCurrent.Columns["dgvcmbUnit"].Index, inRowIndex];
        //        dgvCurrent[dgvCurrent.Columns["dgvcmbUnit"].Index, inRowIndex].Value = null;
        //        if (dtblUnitViewAll.Rows.Count > 0)
        //        {
        //            dgvcmbUnit.DataSource = dtblUnitViewAll;
        //            dgvcmbUnit.DisplayMember = "unitName";
        //            dgvcmbUnit.ValueMember = "unitId";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //         ex.ToString();
        //    }
        //    return dtblUnitViewAll;
        //}
        /// <summary>
        /// Function to fill AccountLedgers for String
        /// </summary>
        /// <returns></returns>
        //public DataTable AccountLedgerComboFill()
        //{
        //    DataTable dtbl = new DataTable();
        //    try
        //    {
        //        AccountLedgerSP spaccountledger = new AccountLedgerSP();
        //        dtbl = spaccountledger.AccountLedgerViewAll();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("TGF:" + ex.Message, StrResources.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
        //    return dtbl;
       // }
        /// <summary>
        /// Function to fill All cash/Bank ledgers for String
        /// </summary>
        /// <param name="isAll"></param>
        /// <returns></returns>
        //public DataTable BankOrCashComboFill(bool isAll)
        //{
        //    DataTable dtbl = new DataTable();
        //    SqlDataAdapter sdaadapter = new SqlDataAdapter("CashOrBankComboFill", Sqlcon);
        //    try
        //    {
        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        sdaadapter.Fill(dtbl);
        //    }
        //    catch (Exception ex)
        //    {
        //         ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //    return dtbl;
        //}
        /// <summary>
        /// Function to check the status of PrintAfterSave and returns the status
        /// </summary>
        /// <returns></returns>
        public bool StatusOfPrintAfterSave()
        {
            string strStatus = "";
            bool isTrue = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("PrintAfterSave", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                strStatus = sccmd.ExecuteScalar().ToString();
                if (strStatus == "Yes")
                {
                    isTrue = true;
                }
                else
                {
                    isTrue = false;
                }
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isTrue;
        }
        /// <summary>
        /// Function to check the status of Tax and return the status
        /// </summary>
        /// <returns></returns>
        public bool TaxStatus()
        {
            string strStatus = "";
            bool isTrue = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("TaxStatus", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                strStatus = sccmd.ExecuteScalar().ToString();
                if (strStatus == "Yes")
                {
                    isTrue = true;
                }
                else
                {
                    isTrue = false;
                }
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isTrue;
        }
        /// <summary>
        /// Function to check the status of Godown and return the status
        /// </summary>
        /// <returns></returns>
        public bool GodownStatus()
        {
            string strStatus = "";
            bool isTrue = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("GodownStatus", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                strStatus = sccmd.ExecuteScalar().ToString();
                if (strStatus == "Yes")
                {
                    isTrue = true;
                }
                else
                {
                    isTrue = false;
                }
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isTrue;
        }
        /// <summary>
        /// Function to fill All Currencies created On Each Date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public DataTable CurrencyComboByDate(DateTime date)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("CurrencyComboByDate", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlparameter = new SqlParameter();
                sqlparameter = sdaadapter.SelectCommand.Parameters.Add("@date", SqlDbType.DateTime);
                sqlparameter.Value = date;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to View all SalesMan for String
        /// </summary>
        /// <param name="cmbSalesMan"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        //public DataTable SalesmanViewAllForComboFill(String cmbSalesMan, bool isAll)
        //{
        //    DataTable dtbl = new DataTable();
        //    try
        //    {
        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        SqlDataAdapter sdaadapter = new SqlDataAdapter("SalesmanViewAllForComboFill", Sqlcon);
        //        sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        sdaadapter.Fill(dtbl);
        //        if (isAll)
        //        {
        //            DataRow dr = dtbl.NewRow();
        //            dr["employeeId"] = 0;
        //            dr["employeeName"] = "All";
        //            dtbl.Rows.InsertAt(dr, 0);
        //        }
        //        cmbSalesMan.DisplayMember = "EmployeeName";
        //        cmbSalesMan.ValueMember = "EmployeeId";
        //        cmbSalesMan.DataSource = dtbl;
        //    }
        //    catch (Exception ex)
        //    {
        //         ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //    return dtbl;
        //}
        /// <summary>
        /// Function to fill all Cash or Party Ledgers created under SundryDebtor for String
        /// </summary>
        /// <param name="cmbCashOrParty"></param>
        /// <param name="isAll"></param>
        public void CashOrPartyUnderSundryDrComboFill(String cmbCashOrParty, bool isAll)
        {
            // Fill All  Cash or party Under Sundry Debter
            DataTable dtblCashOrParty = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("CashOrPartyUnderSundryDrComboFill", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtblCashOrParty);
                if (isAll)
                {
                    DataRow dr = dtblCashOrParty.NewRow();
                    dr["ledgerName"] = "All";
                    dr["ledgerId"] = 0;
                    dtblCashOrParty.Rows.InsertAt(dr, 0);
                }
              //  cmbCashOrParty.DataSource = dtblCashOrParty;
               // cmbCashOrParty.ValueMember = "ledgerId";
               // cmbCashOrParty.DisplayMember = "ledgerName";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to fill all PricingLevel for String
        /// </summary>
        /// <param name="cmbPricingLevel"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable PricingLevelViewAll(String cmbPricingLevel, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("PricingLevelViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            //   cmbPricingLevel.DataSource = dtbl;
              //  cmbPricingLevel.DisplayMember = "pricinglevelName";
               // cmbPricingLevel.ValueMember = "pricinglevelId";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all Ledgers created under SalesAccount for String 
        /// </summary>
        /// <param name="cmbSalesAccount"></param>
        /// <param name="isAll"></param>
        public void SalesAccountComboFill(String cmbSalesAccount, bool isAll)
        {
            DataTable dtblSalesAccount = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("SalesAccountComboFill", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtblSalesAccount);
                if (isAll)
                {
                    DataRow dr = dtblSalesAccount.NewRow();
                    dr["ledgerName"] = "All";
                    dr["ledgerId"] = 0;
                    dtblSalesAccount.Rows.InsertAt(dr, 0);
                }
               // cmbSalesAccount.DataSource = dtblSalesAccount;
               // cmbSalesAccount.DisplayMember = "ledgerName";
               // cmbSalesAccount.ValueMember = "ledgerId";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to fill all ProductGroup for String
        /// </summary>
        /// <param name="cmbProductGroup"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable ProductGroupViewAll(String cmbProductGroup, bool isAll)
        {
            DataTable dtblProductGroup = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("ProductGroupViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtblProductGroup);
                if (isAll)
                {
                    DataRow dr = dtblProductGroup.NewRow();
                    dr["groupName"] = "All";
                    dr["groupId"] = 0;
                    dtblProductGroup.Rows.InsertAt(dr, 0);
                }
               // //cmbProductGroup.DataSource = dtblProductGroup;
               // cmbProductGroup.ValueMember = "groupId";
               // cmbProductGroup.DisplayMember = "groupName";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtblProductGroup;
        }
        /// <summary>
        /// Function to fill all Units for String
        /// </summary>
        /// <param name="cmbUnit"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable UnitViewAll(String cmbUnit, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("UnitViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dtbl.Columns.Add("SLNO", typeof(decimal));
                dtbl.Columns["SLNO"].AutoIncrement = true;
                dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
                dtbl.Columns["SLNO"].AutoIncrementStep = 1;
                sdaadapter.Fill(dtbl);
                if (isAll)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["unitId"] = 0;
                    dr["unitName"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);
                }
             //   cmbUnit.DataSource = dtbl;
             //   cmbUnit.ValueMember = "unitId";
             //   cmbUnit.DisplayMember = "unitName";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all Tax for String
        /// </summary>
        /// <param name="cmbTax"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable TaxViewAll(String cmbTax, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("TaxViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
                if (isAll)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["taxId"] = 0;
                    dr["taxName"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);
                }
             //   cmbTax.DataSource = dtbl;
             //   cmbTax.ValueMember = "taxId";
              //  cmbTax.DisplayMember = "taxName";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all Brand for String
        /// </summary>
        /// <param name="cmbBrand"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable BrandViewAll(String cmbBrand, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("BrandViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dtbl.Columns.Add("SLNO", typeof(decimal));
                dtbl.Columns["SLNO"].AutoIncrement = true;
                dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
                dtbl.Columns["SLNO"].AutoIncrementStep = 1;
                sdaadapter.Fill(dtbl);
                if (isAll)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["brandId"] = 0;
                    dr["brandName"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);
                }
            //    cmbBrand.DataSource = dtbl;
             ////   cmbBrand.ValueMember = "brandId";
              //  cmbBrand.DisplayMember = "brandName";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all ModelNo for String
        /// </summary>
        /// <param name="cmbModelNo"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable ModelNoViewAll(String cmbModelNo, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("ModelNoViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
                if (isAll)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["modelNoId"] = 0;
                    dr["modelNo"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);
                }
              //  cmbModelNo.DataSource = dtbl;
              //  cmbModelNo.ValueMember = "modelNoId";
              //  cmbModelNo.DisplayMember = "modelNo";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all Tax for String
        /// </summary>
        /// <param name="cmbSize"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable SizeViewAll(String cmbSize, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("SizeViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
                if (isAll)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["sizeId"] = 0;
                    dr["size"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);
                }
             //   cmbSize.DataSource = dtbl;
               // cmbSize.ValueMember = "sizeId";
               // cmbSize.DisplayMember = "size";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all Bank ledgers for String
        /// </summary>
        /// <returns></returns>
        public DataTable BankComboFill()
        {
            DataTable dtblAccount = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("BankAccountComboFill", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtblAccount);
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtblAccount;
        }
        /// <summary>
        /// Function to fill all Routes for String
        /// </summary>
        /// <param name="cmbSize"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable RouteViewAll(String cmbSize, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("RouteViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
                if (isAll)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["routeId"] = 0;
                    dr["routeName"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);
                }
              //  cmbSize.DataSource = dtbl;
             //   cmbSize.ValueMember = "routeId";
             //   cmbSize.DisplayMember = "routeName";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to fill all Area for String
        /// </summary>
        /// <param name="cmbSize"></param>
        /// <param name="isAll"></param>
        /// <returns></returns>
        public DataTable AreaViewAll(String cmbSize, bool isAll)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("AreaViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
                if (isAll)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["areaId"] = 0;
                    dr["areaName"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);
                }
              //  cmbSize.DataSource = dtbl;
              //  cmbSize.ValueMember = "areaId";
               // cmbSize.DisplayMember = "areaName";
            }
            catch (Exception ex)
            {
                 ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
    }
}
