
using System;    
   
namespace AccuSale_Final.Models
{    
public class BatchInfo    
{    
    public decimal _batchId;    
    public string _batchNo;    
    public decimal _productId;    
    public DateTime _manufacturingDate;    
    public DateTime _expiryDate;    
    public string Extra1=string.Empty;    
    public string Extra2 = string.Empty;    
    public DateTime ExtraDate=DateTime.Now;
    public string _narration;
    public string _barcode;
    public string _partNo;
    public decimal BatchId    
    {    
        get { return _batchId; }    
        set { _batchId = value; }    
    }    
    public string BatchNo    
    {    
        get { return _batchNo; }    
        set { _batchNo = value; }    
    }    
    public decimal ProductId    
    {    
        get { return _productId; }    
        set { _productId = value; }    
    }    
    public DateTime ManufacturingDate    
    {    
        get { return _manufacturingDate; }    
        set { _manufacturingDate = value; }    
    }    
    public DateTime ExpiryDate    
    {    
        get { return _expiryDate; }    
        set { _expiryDate = value; }    
    }
 
    public string narration
    {
        get { return _narration; }
        set { _narration = value; }
    }
    public string barcode
    {
        get { return _barcode; }
        set { _barcode = value; }
    }
    public string partNo
    {
        get { return _partNo; }
        set { _partNo = value; }
    }

}    
}
