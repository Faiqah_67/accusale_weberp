﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Globalization;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class CompanyController : Controller
    {
        CompanySP spCompany= new CompanySP();
        CurrencySP spcurrency= new CurrencySP();
        // GET: Company
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddCompany(CompanyInfo companyinfo,string FinancialYearFrom, string BooksBeginingFrom)
        {
            var outputmsg = string.Empty;

            try
            {

                companyinfo.FinancialYearFrom = Convert.ToDateTime(FinancialYearFrom);
                companyinfo.BooksBeginingFrom = Convert.ToDateTime(BooksBeginingFrom);

                if (companyinfo.CompanyId == 0) {
                    if (spCompany.CompanyCheckExistence(companyinfo.CompanyName.Trim().ToString(), 0) == false)
                    {
                        spCompany.CompanyAdd(companyinfo); outputmsg = "Company created successfully";
                    }
                    else { outputmsg = "Company already exists"; }

                }
                else {
                    spCompany.CompanyEdit(companyinfo);
                    outputmsg = "Company updated successfully";
                }
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.ToString() });
            }

            return Json(new { message = outputmsg });
        }

        [HttpPost]
        public JsonResult DeleteCompany(int CompanyId)
        {
            spCompany.CompanyDelete(CompanyId);
            return Json(1);
        }
        [HttpGet]
        public JsonResult ShowCompany()
        {
            DataTable dt = spCompany.CompanyViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditCompany(int CompanyId)
        {
            CompanyInfo companyinfo = spCompany.CompanyView(CompanyId);
        
            string FinancialYearFrom = companyinfo.FinancialYearFrom.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            string BooksBeginingFrom = companyinfo.BooksBeginingFrom.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);

            return Json(new {  companyinfo, FinancialYearFrom , BooksBeginingFrom }, JsonRequestBehavior.AllowGet);
        }
      
    }
}