
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;
//<summary>    
//Summary description for ServiceSP    
//</summary>    
namespace AccuSale_Final.DAL
{
    class ServiceSP 
    {
        #region Functions
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public bool ServiceAdd(ServiceInfo serviceinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceName", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.ServiceName;
                sprmparam = sccmd.Parameters.Add("@serviceCategoryId", SqlDbType.Decimal);
                sprmparam.Value = serviceinfo.ServiceCategoryId;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = serviceinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Extra2;
                int inEffectedRow = sccmd.ExecuteNonQuery();
                if (inEffectedRow > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in Service Table
        /// </summary>
        /// <param name="serviceinfo"></param>
        /// <returns></returns>
        public bool ServiceEdit(ServiceInfo serviceinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceId", SqlDbType.Decimal);
                sprmparam.Value = serviceinfo.ServiceId;
                sprmparam = sccmd.Parameters.Add("@serviceName", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.ServiceName;
                sprmparam = sccmd.Parameters.Add("@serviceCategoryId", SqlDbType.Decimal);
                sprmparam.Value = serviceinfo.ServiceCategoryId;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = serviceinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Extra2;
                int inEffectedRow = sccmd.ExecuteNonQuery();
                if (inEffectedRow > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from Service Table
        /// </summary>
        /// <returns></returns>
        public DataTable ServiceViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("ServiceViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dtbl.Columns.Add("SLNO", typeof(decimal));
                dtbl.Columns["SLNO"].AutoIncrement = true;
                dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
                dtbl.Columns["SLNO"].AutoIncrementStep = 1;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from Service Table based on the parameter
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public ServiceInfo ServiceView(decimal serviceId)
        {
            ServiceInfo serviceinfo = new ServiceInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceId", SqlDbType.Decimal);
                sprmparam.Value = serviceId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    serviceinfo.ServiceId = decimal.Parse(sdrreader[0].ToString());
                    serviceinfo.ServiceName = sdrreader["serviceName"].ToString();
                    serviceinfo.ServiceCategoryId = decimal.Parse(sdrreader["serviceCategoryId"].ToString());
                    serviceinfo.Rate = decimal.Parse(sdrreader["rate"].ToString());
                    serviceinfo.Narration = sdrreader["narration"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return serviceinfo;
        }
        /// <summary>
        /// Function to get particular values from Service Table based on the parameter
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public ServiceInfo ServiceViewForRate(decimal serviceId)
        {
            ServiceInfo serviceinfo = new ServiceInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceViewForRate", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceId", SqlDbType.Decimal);
                sprmparam.Value = serviceId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    serviceinfo.ServiceName = sdrreader["serviceName"].ToString();
                    serviceinfo.ServiceCategoryId = decimal.Parse(sdrreader["serviceCategoryId"].ToString());
                    serviceinfo.Rate = decimal.Parse(sdrreader["rate"].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return serviceinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="ServiceId"></param>
        public void ServiceDelete(decimal ServiceId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceId", SqlDbType.Decimal);
                sprmparam.Value = ServiceId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for Service Table
        /// </summary>
        /// <returns></returns>
        public int ServiceGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to check existence of Service based on parameters
        /// </summary>
        /// <param name="strServiceName"></param>
        /// <param name="decServiceId"></param>
        /// <returns></returns>
        public bool ServiceCheckExistence(string strServiceName, decimal decServiceId)
        {
            bool isEdit = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceCheckExistence", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceName", SqlDbType.VarChar);
                sprmparam.Value = strServiceName;
                sprmparam = sccmd.Parameters.Add("@serviceId", SqlDbType.Decimal);
                sprmparam.Value = decServiceId;
                object obj = sccmd.ExecuteScalar();
                if (obj != null)
                {
                    if (int.Parse(obj.ToString()) == 0)
                    {
                        isEdit = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
            finally
            {
                Sqlcon.Close();
            }
            return isEdit;
        }
        /// <summary>
        /// Function to view all details for Gridfill
        /// </summary>
        /// <returns></returns>
        public DataTable ServiceGridFill()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("ServiceGridFill", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dtbl.Columns.Add("SLNO", typeof(decimal));
                dtbl.Columns["SLNO"].AutoIncrement = true;
                dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
                dtbl.Columns["SLNO"].AutoIncrementStep = 1;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to view all details for Search based on parameter
        /// </summary>
        /// <param name="strBrandName"></param>
        /// <param name="strCategoryname"></param>
        /// <returns></returns>
        //public DataTable ServiceSearch(string strBrandName, string strCategoryname)
        //{
        //    EmployeeInfo infoEmployee = new EmployeeInfo();
        //    DataTable dtbl = new DataTable();
        //    try
        //    {
        //        SqlDataAdapter sqlda = new SqlDataAdapter("ServiceSearch", Sqlcon);
        //        sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        dtbl.Columns.Add("SLNO", typeof(decimal));
        //        dtbl.Columns["SLNO"].AutoIncrement = true;
        //        dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
        //        dtbl.Columns["SLNO"].AutoIncrementStep = 1;
        //        sqlda.SelectCommand.Parameters.Add("@serviceName", SqlDbType.VarChar).Value = strBrandName;
        //        sqlda.SelectCommand.Parameters.Add("@categoryName", SqlDbType.VarChar).Value = strCategoryname;
        //        sqlda.Fill(dtbl);
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }
        //    return dtbl;
        //}
        /// <summary>
        /// Function to check reference for delete
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <returns></returns>
        public decimal ServiceDeleteReferenceCheck(decimal ServiceId)
        {
            decimal decReturnValue = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceDeleteReference", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceId", SqlDbType.Decimal);
                sprmparam.Value = ServiceId;
                decReturnValue = Convert.ToDecimal(sccmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decReturnValue;
        }
        /// <summary>
        /// Function to insert values to Service Table and return the Curresponding row's Id
        /// </summary>
        /// <param name="serviceinfo"></param>
        /// <returns></returns>
        public decimal ServiceAddWithReturnIdentity(ServiceInfo serviceinfo)
        {
            decimal decIdentity = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ServiceAddWithReturnIdentity", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@serviceName", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.ServiceName;
                sprmparam = sccmd.Parameters.Add("@serviceCategoryId", SqlDbType.Decimal);
                sprmparam.Value = serviceinfo.ServiceCategoryId;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = serviceinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = serviceinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = serviceinfo.Extra2;
                decIdentity = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decIdentity;
        }
        #endregion
    }
}
