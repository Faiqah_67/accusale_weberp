﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class GoDownController : Controller
    {
        GodownSP GodownSP = new GodownSP();

        // GET: GoDown
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddGoDown(GodownInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.GodownId == 0)
            {
                if (GodownSP.GodownCheckIfExist(datainfo.GodownName, 0) == false)
                {
                    GodownSP.GodownAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.GodownId != 0)
            {
                GodownSP.GodownEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteGoDown(int GodownId)
        {
            GodownSP.GodownDelete(GodownId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = GodownSP.GodownViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int GodownId)
        {
            GodownInfo datainfo = GodownSP.GodownView(GodownId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}