﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using System.Web.Mvc;

namespace AccuSale_Final.Controllers
{
    public class HomeController : Controller
    {
        UserSP spUser = new UserSP();
        UserInfo Userinfo = new UserInfo();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoginCheck(string UserName)
        {
            Userinfo.Password = spUser.LoginCheck(UserName);
            return Json(Userinfo.Password);
        }

        [HttpPost]
        public ActionResult Login(UserInfo user)
        {

            string strPassword = spUser.LoginCheck(user.UserName);
            if (strPassword == user.Password.Trim())
            {
                user.UserId = spUser.Login(user);
                Session["UserId"] = user.UserId;
                Session["UserName"] = user.UserName;
                return Json(Url.Action("Index", "Menu"));

            }
            else
            {
                return Json(Url.Action("Index", "Home"));


            }


        }

  
    }
}