﻿using Accusale_Final.DAL;
using Accusale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class BrandController : Controller
    {
        BrandSP BrandSP = new BrandSP();

        // GET: Brand
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddBrand(BrandInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.BrandId == 0)
            {
                if (BrandSP.BrandCheckIfExist(datainfo.BrandName, 0) == false)
                {
                    BrandSP.BrandAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.BrandId != 0)
            {
                BrandSP.BrandEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteBrand(int BrandId)
        {
            BrandSP.BrandDelete(BrandId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = BrandSP.BrandViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int brandId)
        {
            BrandInfo datainfo = BrandSP.BrandView(brandId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}