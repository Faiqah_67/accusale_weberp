﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Accusale_WebERP.Controllers
{
    public class RoleController : Controller
    {
        RoleSP rolesp = new RoleSP();
        RoleInfo roleinfo = new RoleInfo();
        // GET: Role
        public ActionResult Index()
        {
            return View();
        }
    
        [HttpPost]
        public JsonResult RoleAdd(RoleInfo roleinfo)
        {
            var outputmsg = string.Empty;


            if (roleinfo.RoleId == 0) {
                if (rolesp.RoleCheckExistence(roleinfo.RoleId, roleinfo.Role) == false)
                {
                    rolesp.RoleAdd(roleinfo);
                    outputmsg = "Role created successfully";
                }
                else
                {
                    outputmsg = "Role already exists" ;

                }
                }


                if (roleinfo.RoleId != 0) {
                rolesp.RoleEdit(roleinfo); outputmsg = "Role updated successfully";
            }


            return Json(new { message = outputmsg });


        }

        [HttpPost]
        public JsonResult RoleDelete(int roleid)
        {
            rolesp.RoleReferenceDelete(roleid);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowRole()
        {
            DataTable dt = rolesp.RoleViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new {json }, JsonRequestBehavior.AllowGet);
        }

    }
}