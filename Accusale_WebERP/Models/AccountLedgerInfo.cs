
using System;    
   
   
namespace AccuSale_Final.Models    
{    
public class AccountLedgerInfo    
{    
    public decimal _ledgerId;    
    public decimal _accountGroupId;    
    public string _LedgerCode;
    public string _ledgerName;    
    public decimal _openingBalance;    
    public string _crOrDr;    
    public string _narration;    
    public string _mailingName;    
    public string _address;    
    public string _state;    
    public string _phone;
    public string _mobile;    
    public string _email;    
    public int _creditPeriod;    
    public decimal _creditLimit;
    public decimal _pricinglevelId;    
    public bool _billByBill;    
    public string _tin;    
    public string _cst;    
    public string _pan;    
    public decimal _routeId;    
    public string _bankAccountNumber;    
    public string _branchName;    
    public string _branchCode;    
    public DateTime ExtraDate=DateTime.Now;    
    public string _extra1;    
    public string _extra2;    
    public decimal _areaId;
    public bool _isDefault;  
    
    public decimal LedgerId    
    {    
        get { return _ledgerId; }    
        set { _ledgerId = value; }    
    }    
    public decimal AccountGroupId    
    {    
        get { return _accountGroupId; }    
        set { _accountGroupId = value; }    
    }    
    public string LedgerCode   
    {    
        get { return _LedgerCode; }    
        set { _LedgerCode = value; }    
    }
    public string LedgerName
    {
        get { return _ledgerName; }
        set { _ledgerName = value; }
    }
        public decimal OpeningBalance    
    {    
        get { return _openingBalance; }    
        set { _openingBalance = value; }    
    }    
    public string CrOrDr    
    {    
        get { return _crOrDr; }    
        set { _crOrDr = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
    public string MailingName    
    {    
        get { return _mailingName; }    
        set { _mailingName = value; }    
    }    
    public string Address    
    {    
        get { return _address; }    
        set { _address = value; }    
    }    
    public string State    
    {    
        get { return _state; }    
        set { _state = value; }    
    }    
    public string Phone    
    {    
        get { return _phone; }    
        set { _phone = value; }    
    }    
    public string Mobile 
    {
        get { return _mobile; }
        set { _mobile = value; }    
    }    
    public string Email    
    {    
        get { return _email; }    
        set { _email = value; }    
    }    
    public int CreditPeriod    
    {    
        get { return _creditPeriod; }    
        set { _creditPeriod = value; }    
    }    
    public decimal CreditLimit    
    {    
        get { return _creditLimit; }    
        set { _creditLimit = value; }    
    }
    public decimal PricinglevelId    
    {
        get { return _pricinglevelId; }
        set { _pricinglevelId = value; }    
    }    
    public bool BillByBill    
    {    
        get { return _billByBill; }    
        set { _billByBill = value; }    
    }    
    public string Tin    
    {    
        get { return _tin; }    
        set { _tin = value; }    
    }    
    public string Cst    
    {    
        get { return _cst; }    
        set { _cst = value; }    
    }    
    public string Pan    
    {    
        get { return _pan; }    
        set { _pan = value; }    
    }    
    public decimal RouteId    
    {    
        get { return _routeId; }    
        set { _routeId = value; }    
    }    
    public string BankAccountNumber    
    {    
        get { return _bankAccountNumber; }    
        set { _bankAccountNumber = value; }    
    }    
    public string BranchName    
    {    
        get { return _branchName; }    
        set { _branchName = value; }    
    }    
    public string BranchCode    
    {    
        get { return _branchCode; }    
        set { _branchCode = value; }    
    }    
    
    public string Extra1    
    {    
        get { return _extra1; }    
        set { _extra1 = value; }    
    }    
    public string Extra2    
    {    
        get { return _extra2; }    
        set { _extra2 = value; }    
    }    
    public decimal AreaId    
    {    
        get { return _areaId; }    
        set { _areaId = value; }    
    }
    public bool IsDefault
    {
        get { return _isDefault; }
        set { _isDefault = value; }
    } 
}    
}
