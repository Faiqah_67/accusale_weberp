﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class ResetPasswordController : Controller
    {
        UserSP spUser = new UserSP();

        // GET: ResetPassword
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ChangePassword(UserInfo user)
        {
            string strPassword = spUser.LoginCheck(user.UserName);
            if (strPassword == user.OPassword.Trim())
            {
                spUser.ChangePasswordEdit(user);
                return Json(new { success = Url.Action("Index", "Home") });

            }
            else
            {
            return Json(new { error = "Invalid password" });

            }



        }
    }
}