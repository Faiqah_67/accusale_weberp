﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class FYController : Controller
    {
        FinancialYearSP financialyearsp = new FinancialYearSP();
        FinancialYearInfo financialyearinfo = new FinancialYearInfo();
        // GET: FY
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveFY(string FinancialYearId, string ToDate, string FromDate)
        {
            var outputmsg =string.Empty;

            try
            {
                financialyearinfo.FinancialYearId = Convert.ToInt32(FinancialYearId);
                financialyearinfo.FromDate = Convert.ToDateTime(FromDate);
                financialyearinfo.ToDate = Convert.ToDateTime(ToDate);
                if (financialyearinfo.FinancialYearId == 0)
                {
                    if (financialyearsp.FinancialYearExistenceCheck(financialyearinfo.FromDate, financialyearinfo.ToDate) == false)
                    {
                        financialyearsp.FinancialYearAdd(financialyearinfo);
                        outputmsg = "Financial year created successfully";
                    }
                    else
                    {
                        outputmsg = "Financial year already exist.";

                    }
                }
                if (financialyearinfo.FinancialYearId != 0)
                { financialyearsp.FinancialYearEdit(financialyearinfo); outputmsg = "Financial year updated successfully"; }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.ToString() });
            }

            return Json(new { message= outputmsg });
        }

        [HttpPost]
        public JsonResult FYDelete(int FYID)
        {
            financialyearsp.FinancialYearDelete(FYID);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowFY()
        {
            DataTable dt = financialyearsp.FinancialYearViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
    }
}