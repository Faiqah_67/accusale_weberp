
using System;

namespace Accusale_Final.Models 
{    
public class BrandInfo    
{    
    public decimal _brandId;    
    public string _brandName;    
    public string _narration;
    public string _manufacturer;
    public DateTime ExtraDate=DateTime.Now;   
    public string _extra1;    
    public string _extra2;
    
    
    public decimal BrandId    
    {    
        get { return _brandId; }    
        set { _brandId = value; }    
    }    
    public string BrandName    
    {    
        get { return _brandName; }    
        set { _brandName = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }
    public string Manufacturer
    {
        get { return _manufacturer; }
        set { _manufacturer = value; }
    }    
    
  

    public string Extra1    
    {    
        get { return _extra1; }    
        set { _extra1 = value; }    
    }    
    public string Extra2    
    {    
        get { return _extra2; }    
        set { _extra2 = value; }    
    }    
    
}    
}
