﻿using Accusale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class ModelNumberController : Controller
    {
        ModelNoSP spModelNo = new ModelNoSP();
        // GET: ModelNumber
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddModel(ModelNoInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.ModelNoId == 0)
            {
                if (spModelNo.ModelCheckIfExist(datainfo.ModelNo, 0) == false)
                {
                    spModelNo.ModelNoAddWithDifferentModelNo(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.ModelNoId != 0)
            {
                spModelNo.ModelNoEditParticularFeilds(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteModel(int ModelNoId)
        {
            spModelNo.ModelNoDelete(ModelNoId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spModelNo.ModelNoViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int ModelNoId)
        {
            ModelNoInfo datainfo = spModelNo.ModelNoView(ModelNoId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}