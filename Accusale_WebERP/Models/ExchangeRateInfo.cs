
using System;    
  
namespace Accusale_Final.Models   
{    
public class ExchangeRateInfo    
{    
    public decimal _exchangeRateId;    
    public decimal _currencyId;    
    public DateTime _date;    
    public decimal _rate;    
    public string _narration;    
    public DateTime _extraDate;    
    public string _extra1;    
    public string _extra2;    
    
    public decimal ExchangeRateId    
    {    
        get { return _exchangeRateId; }    
        set { _exchangeRateId = value; }    
    }    
    public decimal CurrencyId    
    {    
        get { return _currencyId; }    
        set { _currencyId = value; }    
    }    
    public DateTime Date    
    {    
        get { return _date; }    
        set { _date = value; }    
    }    
    public decimal Rate    
    {    
        get { return _rate; }    
        set { _rate = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
    public DateTime ExtraDate    
    {    
        get { return _extraDate; }    
        set { _extraDate = value; }    
    }    
    public string Extra1    
    {    
        get { return _extra1; }    
        set { _extra1 = value; }    
    }    
    public string Extra2    
    {    
        get { return _extra2; }    
        set { _extra2 = value; }    
    }    
    
}    
}
