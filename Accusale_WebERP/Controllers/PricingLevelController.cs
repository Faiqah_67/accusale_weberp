﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class PricingLevelController : Controller
    {
        PricingLevelSP PricingLevelSP = new PricingLevelSP();

        // GET: PricingLevel
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddPricinglevel(PricingLevelInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.PricinglevelId == 0)
            {
                if (PricingLevelSP.PricingLevelCheckIfExist(datainfo.PricinglevelName, 0) == false)
                {
                    PricingLevelSP.PricingLevelAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.PricinglevelId != 0)
            {
                PricingLevelSP.PricingLevelEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeletePricinglevel(int PricinglevelId)
        {
            PricingLevelSP.PricingLevelDelete(PricinglevelId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = PricingLevelSP.PricingLevelViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int PricinglevelId)
        {
            PricingLevelInfo datainfo = PricingLevelSP.PricingLevelView(PricinglevelId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}