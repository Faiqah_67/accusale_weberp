﻿using Accusale_Final.DAL;
using Accusale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class ProductGroupController : Controller
    {
        ProductGroupSP spProductGroup = new ProductGroupSP();

        // GET: ProductGroup
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddPGroup(ProductGroupInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.GroupId == 0)
            {
                if (spProductGroup.ProductGroupCheckExistence(datainfo.GroupName,datainfo.GroupId) == false)
                {
                    spProductGroup.ProductGroupAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.GroupId != 0)
            {
                spProductGroup.ProductGroupEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeletePGroup(int GroupId)
        {
            spProductGroup.ProductGroupDelete(GroupId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spProductGroup.ProductGroupViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int groupId)
        {
            ProductGroupInfo datainfo = spProductGroup.ProductGroupView(groupId);


            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ShowUnder()
        {
            DataTable dt = spProductGroup.ProductGroupViewForComboFillForProductGroup();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
    }
}