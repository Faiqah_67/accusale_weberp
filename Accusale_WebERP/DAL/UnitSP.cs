
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;
  
namespace AccuSale_Final.DAL
{
    class UnitSP
    {
        #region Functions
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public decimal UnitAdd(UnitInfo unitinfo)
        {
            decimal decIdentity = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitName", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.UnitName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@noOfDecimalplaces", SqlDbType.Decimal);
                sprmparam.Value = unitinfo.noOfDecimalplaces;
                sprmparam = sccmd.Parameters.Add("@formalName", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.formalName;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.Extra2;
                decIdentity = decimal.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decIdentity;
        }
        /// <summary>
        /// Function to Update values in account group Table
        /// </summary>
        /// <param name="unitinfo"></param>
        /// <returns></returns>
        public bool UnitEdit(UnitInfo unitinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = unitinfo.UnitId;
                sprmparam = sccmd.Parameters.Add("@unitName", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.UnitName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@noOfDecimalplaces", SqlDbType.Decimal);
                sprmparam.Value = unitinfo.noOfDecimalplaces;
                sprmparam = sccmd.Parameters.Add("@formalName", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.formalName;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = unitinfo.Extra2;
                int inEffectedRow = sccmd.ExecuteNonQuery();
                if (inEffectedRow > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
                return false;
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from account group Table
        /// </summary>
        /// <returns></returns>
        public DataTable UnitViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("UnitViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dtbl.Columns.Add("SLNO", typeof(decimal));
                dtbl.Columns["SLNO"].AutoIncrement = true;
                dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
                dtbl.Columns["SLNO"].AutoIncrementStep = 1;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from account group table based on the parameter
        /// </summary>
        /// <param name="unitId"></param>
        /// <returns></returns>
        public UnitInfo UnitView(decimal unitId)
        {
            UnitInfo unitinfo = new UnitInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = unitId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    unitinfo.UnitId = Convert.ToDecimal(sdrreader["unitId"]);
                    unitinfo.UnitName = sdrreader["unitName"].ToString();
                    unitinfo.Narration = sdrreader["narration"].ToString();
                    unitinfo.noOfDecimalplaces = Convert.ToDecimal(sdrreader["noOfDecimalplaces"].ToString());
                    unitinfo.formalName = sdrreader["formalName"].ToString();
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return unitinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="UnitId"></param>
        public void UnitDelete(decimal UnitId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = UnitId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for AdditionalCost table
        /// </summary>
        /// <returns></returns>
        public int UnitGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to get all details for search based on parameter
        /// </summary>
        /// <param name="strUnitName"></param>
        /// <returns></returns>
        //public DataTable UnitSearch(string strUnitName)
        //{
        //    EmployeeInfo infoEmployee = new EmployeeInfo();
        //    DataTable dtbl = new DataTable();
        //    try
        //    {
        //        SqlDataAdapter sqlda = new SqlDataAdapter("UnitSearch", Sqlcon);
        //        sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        dtbl.Columns.Add("SLNO", typeof(decimal));
        //        dtbl.Columns["SLNO"].AutoIncrement = true;
        //        dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
        //        dtbl.Columns["SLNO"].AutoIncrementStep = 1;
        //        sqlda.SelectCommand.Parameters.Add("@unitName", SqlDbType.VarChar).Value = strUnitName;
        //        sqlda.Fill(dtbl);
        //    }
        //    catch (Exception ex)
        //    {
        //       ex.ToString();
        //    }
        //    return dtbl;
        //}
        /// <summary>
        /// Function to check existence of Unit based on parameter
        /// </summary>
        /// <param name="strUnitName"></param>
        /// <param name="decUnitid"></param>
        /// <returns></returns>
        public bool UnitCheckExistence(string strUnitName, decimal decUnitid)
        {
            bool isEdit = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitCheckExistence", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitName", SqlDbType.VarChar);
                sprmparam.Value = strUnitName;
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.VarChar);
                sprmparam.Value = decUnitid;
                object obj = sccmd.ExecuteScalar();
                if (obj != null)
                {
                    if (int.Parse(obj.ToString()) == 0)
                    {
                        isEdit = true;
                    }
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
                return false;
            }
            finally
            {
                Sqlcon.Close();
            }
            return isEdit;
        }
        /// <summary>
        /// Function for unit VieW For StandardRate based on parameter
        /// </summary>
        /// <param name="decProductId"></param>
        /// <returns></returns>
        public UnitInfo unitVieWForStandardRate(decimal decProductId)
        {
            UnitInfo infoUnit = new UnitInfo();
            SqlDataReader sqldr = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("unitVieWForStandardRate", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@productId", SqlDbType.Decimal).Value = decProductId;
                sqldr = sqlcmd.ExecuteReader();
                while (sqldr.Read())
                {
                    infoUnit.UnitId = Convert.ToDecimal(sqldr["unitId"].ToString());
                    infoUnit.UnitName = (sqldr["unitName"].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
                sqldr.Close();
            }
            return infoUnit;
        }
        /// <summary>
        /// Function for Unit view for PriceListPopUp based on parameter
        /// </summary>
        /// <param name="decProductId"></param>
        /// <returns></returns>
        public UnitInfo UnitViewForPriceListPopUp(decimal decProductId)
        {
            UnitInfo infoUnit = new UnitInfo();
            SqlDataReader sqldr = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("UnitViewForPriceListPopUp", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@productId", SqlDbType.Decimal).Value = decProductId;
                sqldr = sqlcmd.ExecuteReader();
                while (sqldr.Read())
                {
                    infoUnit.UnitId = Convert.ToDecimal(sqldr["unitId"].ToString());
                    infoUnit.UnitName = (sqldr["unitName"].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
                sqldr.Close();
            }
            return infoUnit;
        }
        /// <summary>
        /// Function to check refernce and delete based on parameter
        /// </summary>
        /// <param name="UnitId"></param>
        /// <returns></returns>
        public decimal UnitDeleteCheck(decimal UnitId)
        {
            decimal decReturnValue = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitDeleteCheckexistence", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = UnitId;
                decReturnValue = decimal.Parse(sccmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decReturnValue;
        }
        /// <summary>
        /// Function to get Unit name based on parameter
        /// </summary>
        /// <param name="UnitId"></param>
        /// <returns></returns>
        public string UnitName(decimal UnitId)
        {
            string strReturnValue = "";
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitName", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = UnitId;
                strReturnValue = Convert.ToString((sccmd.ExecuteScalar()));
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return strReturnValue;
        }
        /// <summary>
        /// Function to view unit based on parameter
        /// </summary>
        /// <param name="UnitName"></param>
        /// <returns></returns>
        public decimal UnitIdByUnitName(string UnitName)
        {
            decimal decUnitId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitIdBYUnitName", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitName", SqlDbType.VarChar);
                sprmparam.Value = UnitName;
                decUnitId = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decUnitId;
        }
        /// <summary>
        /// Function to vioew all unit based on parameter
        /// </summary>
        /// <param name="decUnitId"></param>
        /// <returns></returns>
        public DataTable UnitViewAllWithoutPerticularId(decimal decUnitId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitViewAllWithoutPerticularId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = decUnitId;
                SqlDataAdapter sdaadapter = new SqlDataAdapter("UnitViewAllWithoutPerticularId", Sqlcon);
                sdaadapter.SelectCommand = sccmd;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function for unit combofill based on parameter
        /// </summary>
        /// <param name="dgvCurrent"></param>
        /// <param name="strProductId"></param>
        /// <param name="inRowIndex"></param>
        /// <returns></returns>
        //public DataTable DGVUnitComboFillByProductId(DataGridView dgvCurrent, string strProductId, int inRowIndex)
        //{
        //    DataTable dtblUnitViewAll = new DataTable();
        //    string strUnitName = string.Empty;
        //    try
        //    {
        //        if (Sqlcon.State == ConnectionState.Closed)
        //        {
        //            Sqlcon.Open();
        //        }
        //        SqlDataAdapter sdaadapter = new SqlDataAdapter("UnitViewAllByProductIdForSalesReturn", Sqlcon);
        //        sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
        //        SqlParameter sqlparameter = new SqlParameter();
        //        sqlparameter = sdaadapter.SelectCommand.Parameters.Add("@productId", SqlDbType.VarChar);
        //        sqlparameter.Value = strProductId;
        //        sdaadapter.Fill(dtblUnitViewAll);
        //    }
        //    catch (Exception ex)
        //    {
        //       ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //    try
        //    {
        //        DataGridViewComboBoxCell dgvcmbUnit = (DataGridViewComboBoxCell)dgvCurrent[dgvCurrent.Columns["dgvCmbUnit"].Index, inRowIndex];
        //        dgvCurrent[dgvCurrent.Columns["dgvCmbUnit"].Index, inRowIndex].Value = null;
        //        if (dtblUnitViewAll.Rows.Count > 0)
        //        {
        //            dgvcmbUnit.DataSource = dtblUnitViewAll;
        //            foreach (DataRow item in dtblUnitViewAll.Rows)
        //            {
        //                strUnitName = item["unitName"].ToString();
        //                if (strUnitName != "NA")
        //                {
        //                    DataRow dr = dtblUnitViewAll.NewRow();
        //                    dr["unitName"] = "NA";
        //                    dr["unitId"] = 1;
        //                    dtblUnitViewAll.Rows.InsertAt(dr, 0);
        //                }
        //                break;
        //            }
        //            dgvcmbUnit.DisplayMember = "unitName";
        //            dgvcmbUnit.ValueMember = "unitId";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //       ex.ToString();
        //    }
        //    finally
        //    {
        //        Sqlcon.Close();
        //    }
        //    return dtblUnitViewAll;
        //}
        ///// <summary>
        ///// Function for Unit Conversion check based on parameter
        ///// </summary>
        ///// <param name="decUnitId"></param>
        ///// <param name="decProductId"></param>
        ///// <returns></returns>
        public string UnitConversionCheck(decimal decUnitId, decimal decProductId)
        {
            string strQuantities = string.Empty;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UnitConversionCheck", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = decUnitId;
                sprmparam = sccmd.Parameters.Add("@productId", SqlDbType.Decimal);
                sprmparam.Value = decProductId;
                strQuantities = Convert.ToString(sccmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return strQuantities;
        }
        /// <summary>
        /// Function to view all based on parameter
        /// </summary>
        /// <param name="decProductId"></param>
        /// <returns></returns>
        public DataTable UnitViewAllByProductId(decimal decProductId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("UnitViewAllByProductId", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlda.SelectCommand.Parameters.Add("@productId", SqlDbType.Decimal);
                sprmparam.Value = decProductId;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        #endregion
    }
}
