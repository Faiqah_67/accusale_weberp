
using AccuSale_Final.Models;
using System;    

using System.Data;
using System.Data.SqlClient;
 
namespace AccuSale_Final.DAL 
{
    class RouteSP 
    {
        #region Function
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void RouteAdd(RouteInfo routeinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@routeName", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.RouteName;
                sprmparam = sccmd.Parameters.Add("@areaId", SqlDbType.Decimal);
                sprmparam.Value = routeinfo.AreaId;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = routeinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in Route Table
        /// </summary>
        /// <param name="routeinfo"></param>
        public void RouteEdit(RouteInfo routeinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@routeId", SqlDbType.Decimal);
                sprmparam.Value = routeinfo.RouteId;
                sprmparam = sccmd.Parameters.Add("@routeName", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.RouteName;
                sprmparam = sccmd.Parameters.Add("@areaId", SqlDbType.Decimal);
                sprmparam.Value = routeinfo.AreaId;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = routeinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from Route Table
        /// </summary>
        /// <returns></returns>
        public DataTable RouteViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("RouteViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from Route table based on the parameter
        /// </summary>
        /// <param name="routeId"></param>
        /// <returns></returns>
        public RouteInfo RouteView(decimal routeId)
        {
            RouteInfo routeinfo = new RouteInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@routeId", SqlDbType.Decimal);
                sprmparam.Value = routeId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    routeinfo.RouteId = decimal.Parse(sdrreader[0].ToString());
                    routeinfo.RouteName = sdrreader[1].ToString();
                    routeinfo.AreaId = decimal.Parse(sdrreader[2].ToString());
                    routeinfo.Narration = sdrreader[3].ToString();
                    routeinfo.ExtraDate = DateTime.Parse(sdrreader[4].ToString());
                    routeinfo.Extra1 = sdrreader[5].ToString();
                    routeinfo.Extra2 = sdrreader[6].ToString();
                }
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return routeinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="RouteId"></param>
        public void RouteDelete(decimal RouteId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@routeId", SqlDbType.Decimal);
                sprmparam.Value = RouteId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for Route table
        /// </summary>
        /// <returns></returns>
        public int RouteGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to get all area
        /// </summary>
        /// <returns></returns>
        public DataTable AreafillInRoute()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("AreafillInRoute", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to check existance of route
        /// </summary>
        /// <param name="strRouteName"></param>
        /// <param name="decRouteId"></param>
        /// <param name="decAreaId"></param>
        /// <returns></returns>
        public bool RouteCheckExistence(String strRouteName, decimal decRouteId, decimal decAreaId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("RouteCheckExistence", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@routeName", SqlDbType.VarChar);
                sprmparam.Value = strRouteName;
                sprmparam = sqlcmd.Parameters.Add("@routeId", SqlDbType.Decimal);
                sprmparam.Value = decRouteId;
                sprmparam = sqlcmd.Parameters.Add("@areaId", SqlDbType.Decimal);
                sprmparam.Value = decAreaId;
                object obj = sqlcmd.ExecuteScalar();
                decimal decCount = 0;
                if (obj != null)
                {
                    decCount = decimal.Parse(obj.ToString());
                }
                if (decCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return false;
        }
        /// <summary>
        /// Function to insert values to Route Table
        /// </summary>
        /// <param name="routeinfo"></param>
        /// <returns></returns>
        public decimal RouteAddParticularFields(RouteInfo routeinfo)
        {
            decimal decAreaId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteAddParticularFields", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@routeName", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.RouteName;
                sprmparam = sccmd.Parameters.Add("@areaId", SqlDbType.Decimal);
                sprmparam.Value = routeinfo.AreaId;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra2;
                object objRouteId = sccmd.ExecuteScalar();
                if (objRouteId != null)
                {
                    decAreaId = decimal.Parse(objRouteId.ToString());
                }
                else
                {
                    decAreaId = 0;
                }
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decAreaId;
        }
        /// <summary>
        /// Function to Update values in Route Table
        /// </summary>
        /// <param name="routeinfo"></param>
        /// <returns></returns>
        public bool RouteEditing(RouteInfo routeinfo)
        {
            bool isEdit = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteEditing", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@routeId", SqlDbType.Decimal);
                sprmparam.Value = routeinfo.RouteId;
                sprmparam = sccmd.Parameters.Add("@routeName", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.RouteName;
                sprmparam = sccmd.Parameters.Add("@areaId", SqlDbType.Decimal);
                sprmparam.Value = routeinfo.AreaId;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = routeinfo.Extra2;
                int ina = sccmd.ExecuteNonQuery();
                if (ina > 0)
                {
                    isEdit = true;
                }
                else
                {
                    isEdit = false;
                }
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isEdit;
        }
        /// <summary>
        /// Function to get details based on parameters
        /// </summary>
        /// <param name="strRouteName"></param>
        /// <param name="strAreaName"></param>
        /// <returns></returns>
        public DataTable RouteSearch(String strRouteName, String strAreaName)
        {
            DataTable dtblRoute = new DataTable();
            dtblRoute.Columns.Add("Sl No", typeof(decimal));
            dtblRoute.Columns["Sl No"].AutoIncrement = true;
            dtblRoute.Columns["Sl No"].AutoIncrementSeed = 1;
            dtblRoute.Columns["Sl No"].AutoIncrementStep = 1;
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("RouteSearch", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.Add("@routeName", SqlDbType.VarChar).Value = strRouteName;
                sqlda.SelectCommand.Parameters.Add("@areaName", SqlDbType.VarChar).Value = strAreaName;
                sqlda.Fill(dtblRoute);
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            return dtblRoute;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="RouteId"></param>
        /// <returns></returns>
        public decimal RouteDeleting(decimal RouteId)
        {
            decimal decId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RouteDeleting", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@routeId", SqlDbType.Decimal);
                sprmparam.Value = RouteId;
                decId = decimal.Parse(sccmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decId;
        }
        /// <summary>
        /// Function to get all route
        /// </summary>
        /// <returns></returns>
        public DataTable RouteViewForComboFill()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter();
                SqlCommand sqlcmd = new SqlCommand("RouteViewForComboFill", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sdaadapter.SelectCommand = sqlcmd;
                sdaadapter.Fill(dtbl);
                DataRow dr = dtbl.NewRow();
                dr["routeId"] = 0;
                dr["routeName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get route based on area
        /// </summary>
        /// <param name="decAreaId"></param>
        /// <returns></returns>
        public DataTable RouteViewByArea(decimal decAreaId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("RouteViewByArea", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.Add("@areaId", SqlDbType.Decimal).Value = decAreaId;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
          ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        #endregion
    }
}
