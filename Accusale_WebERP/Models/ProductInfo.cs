
using System;    
using System.Collections.Generic;    
using System.Text;    

namespace Accusale_Final.Models
{
    public class ProductInfo    
{    
    public decimal _productId;    
    public string _productCode;    
    public string _productName;    
    public decimal _groupId;    
    public decimal _brandId;    
    public decimal _unitId;    
    public decimal _sizeId;    
    public decimal _modelNoId;    
    public decimal _taxId;    
    public string _taxapplicableOn;    
    public decimal _purchaseRate;    
    public decimal _salesRate;    
    public decimal _mrp;    
    public decimal _minimumStock;    
    public decimal _maximumStock;    
    public decimal _reorderLevel;    
    public decimal _godownId;    
    public decimal _rackId;    
    public bool _isallowBatch;    
    public bool _ismultipleunit;    
    public bool _isBom;    
    public bool _isopeningstock;    
    public string _narration;    
    public bool _isActive;    
    public bool _isshowRemember;    
    public string _extra1;    
    public string _extra2;    
    public DateTime _extraDate;    
    public string _partNo;    
    
    public decimal ProductId    
    {    
        get { return _productId; }    
        set { _productId = value; }    
    }    
    public string ProductCode    
    {    
        get { return _productCode; }    
        set { _productCode = value; }    
    }    
    public string ProductName    
    {    
        get { return _productName; }    
        set { _productName = value; }    
    }    
    public decimal GroupId    
    {    
        get { return _groupId; }    
        set { _groupId = value; }    
    }    
    public decimal BrandId    
    {    
        get { return _brandId; }    
        set { _brandId = value; }    
    }    
    public decimal UnitId    
    {    
        get { return _unitId; }    
        set { _unitId = value; }    
    }    
    public decimal SizeId    
    {    
        get { return _sizeId; }    
        set { _sizeId = value; }    
    }    
    public decimal ModelNoId    
    {    
        get { return _modelNoId; }    
        set { _modelNoId = value; }    
    }    
    public decimal TaxId    
    {    
        get { return _taxId; }    
        set { _taxId = value; }    
    }    
    public string TaxapplicableOn    
    {    
        get { return _taxapplicableOn; }    
        set { _taxapplicableOn = value; }    
    }    
    public decimal PurchaseRate    
    {    
        get { return _purchaseRate; }    
        set { _purchaseRate = value; }    
    }    
    public decimal SalesRate    
    {    
        get { return _salesRate; }    
        set { _salesRate = value; }    
    }    
    public decimal Mrp    
    {    
        get { return _mrp; }    
        set { _mrp = value; }    
    }    
    public decimal MinimumStock    
    {    
        get { return _minimumStock; }    
        set { _minimumStock = value; }    
    }    
    public decimal MaximumStock    
    {    
        get { return _maximumStock; }    
        set { _maximumStock = value; }    
    }    
    public decimal ReorderLevel    
    {    
        get { return _reorderLevel; }    
        set { _reorderLevel = value; }    
    }    
    public decimal GodownId    
    {    
        get { return _godownId; }    
        set { _godownId = value; }    
    }    
    public decimal RackId    
    {    
        get { return _rackId; }    
        set { _rackId = value; }    
    }    
    public bool IsallowBatch    
    {    
        get { return _isallowBatch; }    
        set { _isallowBatch = value; }    
    }    
    public bool Ismultipleunit    
    {    
        get { return _ismultipleunit; }    
        set { _ismultipleunit = value; }    
    }    
    public bool IsBom    
    {    
        get { return _isBom; }    
        set { _isBom = value; }    
    }    
    public bool Isopeningstock    
    {    
        get { return _isopeningstock; }    
        set { _isopeningstock = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
    public bool IsActive    
    {    
        get { return _isActive; }    
        set { _isActive = value; }    
    }    
    public bool IsshowRemember    
    {    
        get { return _isshowRemember; }    
        set { _isshowRemember = value; }    
    }    
    public string Extra1    
    {    
        get { return _extra1; }    
        set { _extra1 = value; }    
    }    
    public string Extra2    
    {    
        get { return _extra2; }    
        set { _extra2 = value; }    
    }    
    public DateTime ExtraDate    
    {    
        get { return _extraDate; }    
        set { _extraDate = value; }    
    }    
    public string PartNo    
    {    
        get { return _partNo; }    
        set { _partNo = value; }    
    }    
    
}    
}
