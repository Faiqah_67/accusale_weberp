using System;

namespace AccuSale_Final.Models
{
    public class FinancialYearInfo
    {
        public decimal FinancialYearId;
        public DateTime FromDate;
        public DateTime ToDate;
        public DateTime ExtraDate=DateTime.Now;
        public string Extra1;
        public string Extra2;

    }
}
