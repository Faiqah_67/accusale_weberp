﻿using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AccuSale_Final.DAL
{
    public class UserSP
    {
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);
        public string LoginCheck(string strUserName)
        {
            string strPsw = string.Empty;
            object obj = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LoginCheck", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@userName", SqlDbType.VarChar);
                sprmparam.Value = strUserName;
                obj = sccmd.ExecuteScalar();
                if (obj != null)
                {
                    strPsw = sccmd.ExecuteScalar().ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return strPsw;
        }
        public int Login(UserInfo user)
        {
            int UserId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("GetUserIdAfterLogin", Sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userName",user.UserName);
                cmd.Parameters.AddWithValue("@password", user.Password);
                UserId = int.Parse(cmd.ExecuteScalar().ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Sqlcon.Close();
            }
            return UserId;
        }

        public void ChangePasswordEdit(UserInfo userinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ChangePasswordEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@userId", SqlDbType.Decimal);
                sprmparam.Value = userinfo.UserId;
                sprmparam = sccmd.Parameters.Add("@userName", SqlDbType.VarChar);
                sprmparam.Value = userinfo.UserName;
                sprmparam = sccmd.Parameters.Add("@password", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Password;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Sqlcon.Close();
            }
        }

        public void UserAdd(UserInfo userinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UserAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@userName", SqlDbType.VarChar);
                sprmparam.Value = userinfo.UserName;
                sprmparam = sccmd.Parameters.Add("@password", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Password;
                sprmparam = sccmd.Parameters.Add("@active", SqlDbType.Bit);
                sprmparam.Value = userinfo.Active;
                sprmparam = sccmd.Parameters.Add("@roleId", SqlDbType.Decimal);
                sprmparam.Value = userinfo.RoleId;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        public void UserEdit(UserInfo userinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UserEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@userId", SqlDbType.Decimal);
                sprmparam.Value = userinfo.UserId;
                sprmparam = sccmd.Parameters.Add("@userName", SqlDbType.VarChar);
                sprmparam.Value = userinfo.UserName;
                sprmparam = sccmd.Parameters.Add("@password", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Password;
                sprmparam = sccmd.Parameters.Add("@active", SqlDbType.Bit);
                sprmparam.Value = userinfo.Active;
                sprmparam = sccmd.Parameters.Add("@roleId", SqlDbType.Decimal);
                sprmparam.Value = userinfo.RoleId;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = userinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        public void UserDelete(decimal UserId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UserDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@userId", SqlDbType.Decimal);
                sprmparam.Value = UserId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        public bool UserCreationCheckExistence(decimal decUserId, string strUserName)
        {
            bool isEdit = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("UserCreationCheckExistence", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@userId", SqlDbType.Decimal);
                sprmparam.Value = decUserId;
                sprmparam = sqlcmd.Parameters.Add("@userName", SqlDbType.VarChar);
                sprmparam.Value = strUserName;
                object obj = sqlcmd.ExecuteScalar();
                if (obj != null)
                {
                    if (int.Parse(obj.ToString()) == 1)
                    {
                        isEdit = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isEdit;
        }
        public UserInfo UserView(decimal userId)
        {
            UserInfo userinfo = new UserInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("UserView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@userId", SqlDbType.Decimal);
                sprmparam.Value = userId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    userinfo.UserId = decimal.Parse(sdrreader[0].ToString());
                    userinfo.UserName = sdrreader[1].ToString();
                    userinfo.Password = sdrreader[2].ToString();
                    userinfo.Active = bool.Parse(sdrreader[3].ToString());
                    userinfo.RoleId = decimal.Parse(sdrreader[4].ToString());
                    userinfo.Narration = sdrreader[5].ToString();
                    userinfo.ExtraDate = DateTime.Parse(sdrreader[6].ToString());
                    userinfo.Extra1 = sdrreader[7].ToString();
                    userinfo.Extra2 = sdrreader[8].ToString();
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return userinfo;
        }

        public DataTable UserViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("UserViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
    }
}