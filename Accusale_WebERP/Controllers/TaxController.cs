﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class TaxController : Controller
    {
        TaxSP spTax = new TaxSP();
        // GET: Tax
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddTax(TaxInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.TaxId == 0)
            {
                if (spTax.TaxCheckExistence(0,datainfo.TaxName) == false)
                {
                    spTax.TaxAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.TaxId != 0)
            {
                spTax.TaxEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteTax(int TaxId)
        {
            spTax.TaxDelete(TaxId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spTax.TaxViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int TaxId)
        {

            TaxInfo datainfo = spTax.TaxView(TaxId);

            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}