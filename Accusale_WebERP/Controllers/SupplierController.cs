﻿using Accusale_Final.DAL;
using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class SupplierController : Controller
    {
        AccountLedgerSP spAccountLedger = new AccountLedgerSP();
        // GET: Supplier
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddSupplier(AccountLedgerInfo AccountLedgerInfo)
        {
            var outputmsg = string.Empty;

            try
            {
                if (AccountLedgerInfo.LedgerId == 0)
                {
                  
                    if (spAccountLedger.AccountLedgerCheckExistenceForSalesman(AccountLedgerInfo.LedgerName.Trim().ToString(), 0) == false)
                    {
                        AccountLedgerInfo.LedgerId = spAccountLedger.AccountLedgerAddForCustomer(AccountLedgerInfo);
                        if (AccountLedgerInfo.OpeningBalance > 0)
                        {
                            ledgerPosting(AccountLedgerInfo);
                            if (AccountLedgerInfo.BillByBill == true)
                            {
                                partyBalance(AccountLedgerInfo);
                            }
                        }
                        outputmsg = "Supplier created successfully";
                    }
                    else { outputmsg = "Supplier name already exist"; }

                }
                else
                {
                    spAccountLedger.AccountLedgerEditForSalesman(AccountLedgerInfo);
                    ledgerUpdate(AccountLedgerInfo);
                    if (AccountLedgerInfo.BillByBill == true)
                    {
                        partyBalanceUpdate(AccountLedgerInfo);
                    }
                    else
                    {

                        spAccountLedger.PartyBalanceDeleteByVoucherTypeVoucherNoAndReferenceType(AccountLedgerInfo.LedgerId.ToString(), 1);
                    }
                    outputmsg = "Supplier updated successfully";
                }
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.ToString() });
            }

            return Json(new { message = outputmsg });
        }
        public void ledgerPosting(AccountLedgerInfo AccountLedgerInfo)
        {
            try
            {
                string strfinancialId;
                // decOpeningBlnc = Convert.ToDecimal(txtOpeningBalance.Text);
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(18);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                if (AccountLedgerInfo.CrOrDr == "Dr")
                {
                    infoLedgerPosting.Debit = AccountLedgerInfo.OpeningBalance;
                }
                else
                {
                    infoLedgerPosting.Credit = AccountLedgerInfo.OpeningBalance;
                }
                infoLedgerPosting.VoucherTypeId = 1;
                infoLedgerPosting.VoucherNo = AccountLedgerInfo.LedgerId.ToString();
                infoLedgerPosting.Date = Convert.ToDateTime(strfinancialId.ToString());
                infoLedgerPosting.LedgerId = AccountLedgerInfo.LedgerId;
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.InvoiceNo = AccountLedgerInfo.LedgerId.ToString();
                infoLedgerPosting.YearId = Convert.ToInt32(Session["1"]);
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        public void partyBalance(AccountLedgerInfo AccountLedgerInfo)
        {
            try
            {
                PartyBalanceInfo infoPatryBalance = new PartyBalanceInfo();
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                AccountLedgerSP spLedger = new AccountLedgerSP();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                if (AccountLedgerInfo.OpeningBalance > 0)
                {
                    if (AccountLedgerInfo.BillByBill == true)
                    {
                        infoPatryBalance.Date = DateTime.Now;// session
                        infoPatryBalance.LedgerId = AccountLedgerInfo.LedgerId;
                        infoPatryBalance.VoucherTypeId = 1;
                        infoPatryBalance.VoucherNo = AccountLedgerInfo.LedgerId.ToString();
                        infoPatryBalance.AgainstVoucherTypeId = 0;
                        infoPatryBalance.AgainstVoucherNo = "0";
                        infoPatryBalance.ReferenceType = "New";
                        if (AccountLedgerInfo.CrOrDr == "Dr")
                        {
                            infoPatryBalance.Debit = AccountLedgerInfo.OpeningBalance;
                            infoPatryBalance.Credit = 0;
                        }
                        else
                        {
                            infoPatryBalance.Debit = 0;
                            infoPatryBalance.Credit = AccountLedgerInfo.OpeningBalance;
                        }
                        infoPatryBalance.InvoiceNo = AccountLedgerInfo.LedgerId.ToString();
                        infoPatryBalance.AgainstInvoiceNo = "0";
                        infoPatryBalance.CreditPeriod = 0;
                        infoPatryBalance.ExchangeRateId = spExchangeRate.ExchangerateViewByCurrencyId(1);//session
                        infoPatryBalance.FinancialYearId = 1;//session
                        infoPatryBalance.Extra1 = string.Empty;
                        infoPatryBalance.Extra2 = string.Empty;
                    }
                    spPartyBalance.PartyBalanceAdd(infoPatryBalance);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void ledgerUpdate(AccountLedgerInfo AccountLedgerInfo)
        {
            decimal decOpeningBlnc = 0;
            try
            {
                string strfinancialId;
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(18);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                decimal decLedgerPostingId = 0;
                if (AccountLedgerInfo.OpeningBalance.ToString() != string.Empty)
                {
                    decOpeningBlnc = Convert.ToDecimal(AccountLedgerInfo.OpeningBalance.ToString());
                }
                else
                {
                    decOpeningBlnc = 0;
                }
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                DataTable dtbl = spLedgerPosting.GetLedgerPostingIds(AccountLedgerInfo.LedgerId.ToString(), 1);
                foreach (DataRow dr in dtbl.Rows)
                {
                    decLedgerPostingId = Convert.ToDecimal(dr.ItemArray[0].ToString());
                }
                if (AccountLedgerInfo.CrOrDr == "Dr")
                {
                    infoLedgerPosting.Debit = decOpeningBlnc;
                }
                else
                {
                    infoLedgerPosting.Credit = decOpeningBlnc;
                }
                infoLedgerPosting.LedgerPostingId = decLedgerPostingId;
                infoLedgerPosting.VoucherTypeId = 1;
                infoLedgerPosting.VoucherNo = AccountLedgerInfo.LedgerId.ToString();
                infoLedgerPosting.Date = Convert.ToDateTime(strfinancialId.ToString());
                infoLedgerPosting.LedgerId = AccountLedgerInfo.LedgerId;
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.InvoiceNo = AccountLedgerInfo.LedgerId.ToString();
                infoLedgerPosting.YearId = 1;//session
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                if (dtbl.Rows.Count > 0)
                {
                    if (decOpeningBlnc > 0)
                    {
                        spLedgerPosting.LedgerPostingEdit(infoLedgerPosting);
                    }
                    else
                    {
                        AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                        spAccountLedger.LedgerPostingDeleteByVoucherTypeAndVoucherNo(AccountLedgerInfo.LedgerId.ToString(), 1);
                    }
                }
                else
                {
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        public void partyBalanceUpdate(AccountLedgerInfo AccountLedgerInfo)
        {
            try
            {
                AccountLedgerSP spLedger = new AccountLedgerSP();
                spLedger.PartyBalanceDeleteByVoucherTypeVoucherNoAndReferenceType(AccountLedgerInfo.LedgerId.ToString(), 1);
                if (AccountLedgerInfo.OpeningBalance > 0)
                {
                    partyBalance(AccountLedgerInfo);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        [HttpPost]
        public JsonResult DeleteSupplier(int LedgerId)
        {
            if (spAccountLedger.SupplierCheckreferenceAndDelete(LedgerId) == -1)
            {
                return Json("You can't delete,reference exist");
            }
            else
            {
                spAccountLedger.PartyBalanceDeleteByVoucherTypeVoucherNoAndReferenceType(LedgerId.ToString(), 1);
                spAccountLedger.LedgerPostingDeleteByVoucherTypeAndVoucherNo(LedgerId.ToString(), 1);
                spAccountLedger.AccountLedgerDelete(LedgerId);
                return Json("1");
            }

        }
        [HttpGet]
        public JsonResult ShowSupplier(decimal AreaId, decimal RouteId, string ledgername)
        {
            DataTable dt = spAccountLedger.SupplierSearchAll(AreaId, RouteId, ledgername);
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditSupplier(int LedgerId)
        {
            AccountLedgerInfo Supplierinfo = spAccountLedger.AccountLedgerViewForSupplier(LedgerId);
            return Json(new { Supplierinfo }, JsonRequestBehavior.AllowGet);
        }
    }
}