
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AccuSale_Final.DAL
{
    class FinancialYearSP 
    {
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void FinancialYearAdd(FinancialYearInfo financialyearinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@fromDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.FromDate;
                sprmparam = sccmd.Parameters.Add("@toDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.ToDate;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.ExtraDate;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in FinancialYear Table
        /// </summary>
        /// <param name="financialyearinfo"></param>
        public void FinancialYearEdit(FinancialYearInfo financialyearinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@financialYearId", SqlDbType.Decimal);
                sprmparam.Value = financialyearinfo.FinancialYearId;
                sprmparam = sccmd.Parameters.Add("@fromDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.FromDate;
                sprmparam = sccmd.Parameters.Add("@toDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.ToDate;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = financialyearinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = financialyearinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from FinancialYear Table
        /// </summary>
        /// <returns></returns>
        public DataTable FinancialYearViewAll()
        {
            DataTable dtbl = new DataTable();
            dtbl.Columns.Add("SlNo", typeof(int));
            dtbl.Columns["SlNo"].AutoIncrement = true;
            dtbl.Columns["SlNo"].AutoIncrementSeed = 1;
            dtbl.Columns["SlNo"].AutoIncrementStep = 1;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("FinancialYearViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from FinancialYear  table based on the parameter
        /// </summary>
        /// <param name="financialYearId"></param>
        /// <returns></returns>
        public FinancialYearInfo FinancialYearView(decimal financialYearId)
        {
            FinancialYearInfo financialyearinfo = new FinancialYearInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@financialYearId", SqlDbType.Decimal);
                sprmparam.Value = financialYearId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    financialyearinfo.FinancialYearId = decimal.Parse(sdrreader[0].ToString());
                    financialyearinfo.FromDate = DateTime.Parse(sdrreader[1].ToString());
                    financialyearinfo.ToDate = DateTime.Parse(sdrreader[2].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return financialyearinfo;
        }
        /// <summary>
        /// Function to get particular values from FinancialYear table For AccountLedger based on the parameter
        /// </summary>
        /// <param name="financialYearId"></param>
        /// <returns></returns>
        public FinancialYearInfo FinancialYearViewForAccountLedger(decimal financialYearId)
        {
            FinancialYearInfo financialyearinfo = new FinancialYearInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearViewForAccountLedger", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@financialYearId", SqlDbType.Decimal);
                sprmparam.Value = financialYearId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    financialyearinfo.FinancialYearId = decimal.Parse(sdrreader[0].ToString());
                    financialyearinfo.FromDate = DateTime.Parse(sdrreader[1].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return financialyearinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="FinancialYearId"></param>
        public void FinancialYearDelete(decimal FinancialYearId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@financialYearId", SqlDbType.Decimal);
                sprmparam.Value = FinancialYearId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for FinancialYear table
        /// </summary>
        /// <returns></returns>
        public string FinancialYearGetMax()
        {
            string strMax = string.Empty;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                strMax = sccmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return strMax;
        }
        /// <summary>
        /// Function to insert values and return id 
        /// </summary>
        /// <param name="financialyearinfo"></param>
        /// <returns></returns>
        public decimal FinancialYearAddWithReturnIdentity(FinancialYearInfo financialyearinfo)
        {
            decimal decIdentity = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearAddWithReturnIdentity", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@fromDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.FromDate;
                sprmparam = sccmd.Parameters.Add("@toDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.ToDate;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = financialyearinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = financialyearinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = financialyearinfo.Extra2;
                decIdentity = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decIdentity;
        }
        /// <summary>
        /// Function to check existence based on parameter and return status
        /// </summary>
        /// <param name="dtFromDate"></param>
        /// <param name="dtToDate"></param>
        /// <returns></returns>
        public bool FinancialYearExistenceCheck(DateTime dtFromDate, DateTime dtToDate)
        {
            bool trueOrfalse = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("FinancialYearExistenceCheck", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@fromDate", SqlDbType.DateTime);
                sprmparam.Value = dtFromDate;
                sprmparam = sccmd.Parameters.Add("@toDate", SqlDbType.DateTime);
                sprmparam.Value = dtToDate;
                int inD = Convert.ToInt32(sccmd.ExecuteScalar().ToString());
                if (inD == 0)
                {
                    trueOrfalse = true;
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return trueOrfalse;
        }
  
    }
}
