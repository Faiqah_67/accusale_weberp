using System;    

namespace AccuSale_Final.Models
{    
public class UnitInfo    
{    
    public decimal _unitId;    
    public string _unitName;    
    public string _narration;
    public DateTime ExtraDate = DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2 = string.Empty;
        public decimal _noOfDecimalplaces;
    public string _formalName;

    public decimal UnitId    
    {    
        get { return _unitId; }    
        set { _unitId = value; }    
    }    
    public string UnitName    
    {    
        get { return _unitName; }    
        set { _unitName = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
 
    public decimal noOfDecimalplaces
    {
        get { return _noOfDecimalplaces; }
        set { _noOfDecimalplaces = value; }
    }
    public string formalName
    {
        get { return _formalName; }
        set { _formalName = value; }
    }    
}    
}
