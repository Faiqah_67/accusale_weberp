﻿using Accusale_Final.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class StandardRateController : Controller
    {
        ProductGroupSP spProductGroup = new ProductGroupSP();
        // GET: StandardRate
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult ShowData(string groupId, string strProductCode, string strProductName)
        {
            decimal decgroupId = Convert.ToDecimal(groupId);
            DataTable dt = spProductGroup.ProductAndUnitViewAllForGridFill(decgroupId, strProductCode, strProductName);
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
    }
}