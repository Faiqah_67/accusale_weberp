
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AccuSale_Final.DAL
{
    class SizeSP 
    {
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void SizeAdd(SizeInfo sizeinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("SizeAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@sizeId", SqlDbType.Decimal);
                sprmparam.Value = sizeinfo.SizeId;
                sprmparam = sccmd.Parameters.Add("@size", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Size;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = sizeinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in Size Table
        /// </summary>
        /// <param name="sizeinfo"></param>
        public void SizeEdit(SizeInfo sizeinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("SizeEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@sizeId", SqlDbType.Decimal);
                sprmparam.Value = sizeinfo.SizeId;
                sprmparam = sccmd.Parameters.Add("@size", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Size;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = sizeinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = sizeinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from Size Table
        /// </summary>
        /// <returns></returns>
        public DataTable SizeViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("SizeViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from Size Table based on the parameter
        /// </summary>
        /// <param name="sizeId"></param>
        /// <returns></returns>
        public SizeInfo SizeView(decimal sizeId)
        {
            SizeInfo sizeinfo = new SizeInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("SizeView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@sizeId", SqlDbType.Decimal);
                sprmparam.Value = sizeId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    sizeinfo.SizeId = decimal.Parse(sdrreader[0].ToString());
                    sizeinfo.Size = sdrreader[1].ToString();
                    sizeinfo.Narration = sdrreader[2].ToString();
                    sizeinfo.ExtraDate = DateTime.Parse(sdrreader[3].ToString());
                    sizeinfo.Extra1 = sdrreader[4].ToString();
                    sizeinfo.Extra2 = sdrreader[5].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return sizeinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="SizeId"></param>
        public void SizeDelete(decimal SizeId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("SizeDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@sizeId", SqlDbType.Decimal);
                sprmparam.Value = SizeId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for Size Table
        /// </summary>
        /// <returns></returns>
        public int SizeGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("SizeMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to insert values to Size Table and return the Curresponding row's Id
        /// </summary>
        /// <param name="infoSize"></param>
        /// <returns></returns>
        public decimal SizeAdding(SizeInfo infoSize)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("SizeAdding", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@size", SqlDbType.VarChar).Value = infoSize.Size;
                sqlcmd.Parameters.Add("@narration", SqlDbType.VarChar).Value = infoSize.Narration;
                sqlcmd.Parameters.Add("@extra1", SqlDbType.VarChar).Value = infoSize.Extra1;
                sqlcmd.Parameters.Add("@extra2", SqlDbType.VarChar).Value = infoSize.Extra2;
                decimal deceffectedrow = Convert.ToDecimal(sqlcmd.ExecuteScalar());
                if (deceffectedrow > 0)
                {
                    return deceffectedrow;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return 0;
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in Size Table and return the status
        /// </summary>
        /// <param name="infoSize"></param>
        /// <returns></returns>
        public bool SizeEditing(SizeInfo infoSize)
        {
            bool isEdit = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("SizeEditing", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sizeId", SqlDbType.Decimal).Value = infoSize.SizeId;
                sqlcmd.Parameters.Add("@size", SqlDbType.VarChar).Value = infoSize.Size;
                sqlcmd.Parameters.Add("@narration", SqlDbType.VarChar).Value = infoSize.Narration;
                int ineffectedrow = sqlcmd.ExecuteNonQuery();
                if (ineffectedrow > 0)
                {
                    isEdit = true;
                }
                else
                {
                    isEdit = false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isEdit;
        }
        /// <summary>
        /// Function to get  values from Size Table based on the parameter
        /// </summary>
        /// <param name="decSizeId"></param>
        /// <returns></returns>
        public SizeInfo SizeViewing(decimal decSizeId)
        {
            SizeInfo infoSize = new SizeInfo();
            SqlDataReader sqldr = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("SizeViewing", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@sizeId", SqlDbType.Decimal).Value = decSizeId;
                sqldr = sqlcmd.ExecuteReader();
                while (sqldr.Read())
                {
                    infoSize.SizeId = decimal.Parse(sqldr["SizeId"].ToString());
                    infoSize.Size = sqldr["Size"].ToString();
                    infoSize.Narration = sqldr["Narration"].ToString();
                    infoSize.Extra1 = sqldr["Extra1"].ToString();
                    infoSize.Extra2 = sqldr["Extra2"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                sqldr.Close();
                Sqlcon.Close();
            }
            return infoSize;
        }
        /// <summary>
        /// Function to get all values from Size Table 
        /// </summary>
        /// <returns></returns>
        public DataTable SizeViewAlling()
        {
            DataTable dtblSize = new DataTable();
            dtblSize.Columns.Add("Sl.No", typeof(decimal));
            dtblSize.Columns["Sl.No"].AutoIncrement = true;
            dtblSize.Columns["Sl.No"].AutoIncrementSeed = 1;
            dtblSize.Columns["Sl.No"].AutoIncrementStep = 1;
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("SizeViewAlling", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtblSize);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtblSize;
        }
        /// <summary>
        /// Function to check existence of size based on parameter
        /// </summary>
        /// <param name="strSizeName"></param>
        /// <param name="decSizeId"></param>
        /// <returns></returns>
        public bool SizeNameCheckExistence(String strSizeName, decimal decSizeId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("SizeNameCheckExistence", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@size", SqlDbType.VarChar);
                sprmparam.Value = strSizeName;
                sprmparam = sqlcmd.Parameters.Add("@sizeId", SqlDbType.Decimal);
                sprmparam.Value = decSizeId;
                object obj = sqlcmd.ExecuteScalar();
                decimal decCount = 0;
                if (obj != null)
                {
                    decCount = decimal.Parse(obj.ToString());
                }
                if (decCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return false;
        }
        /// <summary>
        /// Function to delete size based on parameter and return corresponding id
        /// </summary>
        /// <param name="SizeId"></param>
        /// <returns></returns>
        public decimal SizeDeleting(decimal SizeId)
        {
            decimal decId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("SizeDeleting", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@sizeId", SqlDbType.Decimal);
                sprmparam.Value = SizeId;
                decId = decimal.Parse(sccmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decId;
        }
   
    }
}
