

using System;    
   

namespace AccuSale_Final.Models
{    
public class ServiceInfo    
{    
    public decimal _serviceId;    
    public string _serviceName;
    public decimal _serviceCategoryId;    
    public decimal _rate;    
    public string _narration;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2 = string.Empty;    
    
    public decimal ServiceId    
    {    
        get { return _serviceId; }    
        set { _serviceId = value; }    
    }    
    public string ServiceName    
    {    
        get { return _serviceName; }    
        set { _serviceName = value; }    
    }
    public decimal ServiceCategoryId    
    {
        get { return _serviceCategoryId; }
        set { _serviceCategoryId = value; }    
    }    
    public decimal Rate    
    {    
        get { return _rate; }    
        set { _rate = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
   
    
}    
}
