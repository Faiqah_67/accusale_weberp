

using System;    
using System.Collections.Generic;    
using System.Text;    
 
namespace AccuSale_Final.Models   
{    
public class ServiceCategoryInfo    
{    
    public decimal _servicecategoryId;    
    public string _categoryName;    
    public string _narration;    
    public DateTime ExtraDate { get; set; }
        public string Extra1 = string.Empty;    
    public string Extra2 = string.Empty;    
    
    public decimal ServicecategoryId    
    {    
        get { return _servicecategoryId; }    
        set { _servicecategoryId = value; }    
    }    
    public string CategoryName    
    {    
        get { return _categoryName; }    
        set { _categoryName = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
  
}    
}
