using System;   

namespace AccuSale_Final.Models 
{    
public class AccountGroupInfo    
{    
    public decimal _accountGroupId;    
    public string _accountGroupName;    
    public decimal _groupUnder;    
    public string _narration;    
    public bool _isDefault;    
    public string _nature;    
    public string _affectGrossProfit;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2=string.Empty;

    public decimal AccountGroupId    
    {    
        get { return _accountGroupId; }    
        set { _accountGroupId = value; }    
    }    
    public string AccountGroupName    
    {    
        get { return _accountGroupName; }    
        set { _accountGroupName = value; }    
    }    
    public decimal GroupUnder    
    {    
        get { return _groupUnder; }    
        set { _groupUnder = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
    public bool IsDefault    
    {    
        get { return _isDefault; }    
        set { _isDefault = value; }    
    }    
    public string Nature    
    {    
        get { return _nature; }    
        set { _nature = value; }    
    }    
    public string AffectGrossProfit    
    {    
        get { return _affectGrossProfit; }    
        set { _affectGrossProfit = value; }    
    }    
    
    
}    
}
