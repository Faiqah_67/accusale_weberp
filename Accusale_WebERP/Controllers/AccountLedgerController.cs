﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class AccountLedgerController : Controller
    {
        AccountLedgerSP spAccountLedger = new AccountLedgerSP();
        // GET: AccountLedger
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddData(AccountLedgerInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.LedgerId == 0)
            {
                if (spAccountLedger.AccountLedgerCheckExistence(datainfo.LedgerName, 0) == false)
                {
                    spAccountLedger.AccountLedgerAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.LedgerId != 0)
            {
                spAccountLedger.AccountLedgerEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteData(int LedgerId)
        {
            spAccountLedger.AccountLedgerDelete(LedgerId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spAccountLedger.AccountLedgerViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int LedgerId)
        {
            AccountLedgerInfo datainfo = spAccountLedger.AccountLedgerView(LedgerId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }

    }
}