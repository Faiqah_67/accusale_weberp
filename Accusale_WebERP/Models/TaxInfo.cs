using System;    
  
   
namespace AccuSale_Final.Models 
{    
public class TaxInfo    
{    
    public decimal _taxId;    
    public string _taxName;    
    public string _applicableOn;    
    public decimal _rate;    
    public string _calculatingMode;    
    public string _narration;    
    public bool _isActive;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2=string.Empty;    
    
    public decimal TaxId    
    {    
        get { return _taxId; }    
        set { _taxId = value; }    
    }    
    public string TaxName    
    {    
        get { return _taxName; }    
        set { _taxName = value; }    
    }    
    public string ApplicableOn    
    {    
        get { return _applicableOn; }    
        set { _applicableOn = value; }    
    }    
    public decimal Rate    
    {    
        get { return _rate; }    
        set { _rate = value; }    
    }    
    public string CalculatingMode    
    {    
        get { return _calculatingMode; }    
        set { _calculatingMode = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
    public bool IsActive    
    {    
        get { return _isActive; }    
        set { _isActive = value; }    
    }    
  
    
}    
}
