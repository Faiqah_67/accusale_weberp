using AccuSale_Final.Models;
using System;    
using System.Data;
using System.Data.SqlClient;
    
 
namespace AccuSale_Final.DAL
{    
class GodownSP
{
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void GodownAdd(GodownInfo godowninfo)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownAdd", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownName", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.GodownName;
            sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Narration;
            sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Extra1;
            sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Extra2;
            sccmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
    }
    /// <summary>
    /// Function to Update values in Godown Table
    /// </summary>
    /// <param name="godowninfo"></param>
    public void GodownEdit(GodownInfo godowninfo)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownEdit", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
            sprmparam.Value = godowninfo.GodownId;
            sprmparam = sccmd.Parameters.Add("@godownName", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.GodownName;
            sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Narration;
            sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Extra1;
            sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Extra2;
            sccmd.ExecuteNonQuery();       
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
    }
   
    /// <summary>
    /// Function to get all the values from Godown Table
    /// </summary>
    /// <returns></returns>
    public DataTable GodownViewAll()
    {
        DataTable dtbl = new DataTable();
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlDataAdapter sdaadapter = new SqlDataAdapter("GodownViewAll", Sqlcon);
            sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaadapter.Fill(dtbl);
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return dtbl;
    }
    /// <summary>
    /// Function to get particular values from Godown table based on the parameter
    /// </summary>
    /// <param name="godownId"></param>
    /// <returns></returns>
 
    public GodownInfo GodownView(decimal godownId )
    {
        GodownInfo godowninfo =new GodownInfo();
        SqlDataReader sdrreader =null;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownView", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
            sprmparam.Value = godownId;
             sdrreader = sccmd.ExecuteReader();
            while (sdrreader.Read())
            {
                godowninfo.GodownId = Convert.ToDecimal(sdrreader[0].ToString());
                godowninfo.GodownName= sdrreader[1].ToString();
                godowninfo.Narration= sdrreader[2].ToString();
                godowninfo.ExtraDate=Convert.ToDateTime(sdrreader[3].ToString());
                godowninfo.Extra1= sdrreader[4].ToString();
                godowninfo.Extra2= sdrreader[5].ToString();
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {           sdrreader.Close(); 
            Sqlcon.Close();
        }
        return godowninfo;
    }
    /// <summary>
    /// Function to delete particular details based on the parameter
    /// </summary>
    /// <param name="GodownId"></param>
    public void GodownDelete(decimal GodownId)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownDelete", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
            sprmparam.Value = GodownId;
            sccmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
    }
    /// <summary>
    /// Function to  get the next id for Godown table
    /// </summary>
    /// <returns></returns>
    public int GodownGetMax()
    {
        int max = 0;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownMax", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            max = Convert.ToInt32(sccmd.ExecuteScalar().ToString());
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return max;
    }
    /// <summary>
    /// Function to get all the values from Godown Table
    /// </summary>
    /// <returns></returns>
    public DataTable GodownOnlyViewAll()
    {
        DataTable dtbl = new DataTable();
        dtbl.Columns.Add("SL.NO", typeof(decimal));
        dtbl.Columns["SL.NO"].AutoIncrement = true;
        dtbl.Columns["SL.NO"].AutoIncrementSeed = 1;
        dtbl.Columns["SL.NO"].AutoIncrementStep = 1;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlDataAdapter sdaadapter = new SqlDataAdapter("GodownOnlyViewAll", Sqlcon);
            sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaadapter.Fill(dtbl);
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return dtbl;
    }
    /// <summary>
    /// Function to insert values to Godown Table with same name
    /// </summary>
    /// <param name="godowninfo"></param>
    /// <returns></returns>
    public decimal GodownAddWithoutSameName(GodownInfo godowninfo)
    {
       // bool isSave = false;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownAddWithoutSameName", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownName", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.GodownName;
            sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Narration;
            sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Extra1;
            sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Extra2;
            decimal decWork = Convert.ToDecimal( sccmd.ExecuteScalar());
            if (decWork > 0)
            {
               return decWork;
            }
            else
            {
                return 0;
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
            return 0;
        }
        finally
        {
            Sqlcon.Close();
        }
       
    }
    /// <summary>
    /// Function to get particular values from Godown table based on the parameter with narration
    /// </summary>
    /// <param name="decGodownId"></param>
    /// <returns></returns>
    public GodownInfo GodownWithNarrationView(decimal decGodownId)
    {
        GodownInfo godowninfo = new GodownInfo();
        SqlDataReader sdrreader = null;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownWithNarrationView", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
            sprmparam.Value = decGodownId;
            sdrreader = sccmd.ExecuteReader();
            while (sdrreader.Read())
            {
                godowninfo.GodownId = Convert.ToDecimal(sdrreader[0].ToString());
                godowninfo.GodownName = sdrreader[1].ToString();
                godowninfo.Narration = sdrreader[2].ToString();
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            sdrreader.Close();
            Sqlcon.Close();
        }
        return godowninfo;
    }
   /// <summary>
   /// Function to check the existance of godown
   /// </summary>
   /// <param name="strGodownName"></param>
   /// <param name="strGodownId"></param>
   /// <returns></returns>
    public bool GodownCheckIfExist(String strGodownName, decimal strGodownId)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sqlcmd = new SqlCommand("GodownCheckIfExist", Sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sqlcmd.Parameters.Add("@godownName", SqlDbType.VarChar);
            sprmparam.Value = strGodownName;
            sprmparam = sqlcmd.Parameters.Add("@godownId", SqlDbType.Decimal);
            sprmparam.Value = strGodownId;
            object obj = sqlcmd.ExecuteScalar();
            decimal decCount = 0;
            if (obj != null)
            {
                decCount = Convert.ToDecimal(obj.ToString());
            }
            if (decCount > 0)
            {
                return true;
            }
            else
            {
                return false; ;
            }
        }
        catch (Exception ex)
        {
                ex.ToString();
            }
        finally
        {
            Sqlcon.Close();
        }
        return false;
    }
    /// <summary>
    /// Function to Update values in Godown Table
    /// </summary>
    /// <param name="godowninfo"></param>
    /// <returns></returns>
    public bool GodownEditParticularField(GodownInfo godowninfo)
    {
        bool isEdit = false;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("godownEditParticularField", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
            sprmparam.Value = godowninfo.GodownId;
            sprmparam = sccmd.Parameters.Add("@godownName", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.GodownName;
            sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
            sprmparam.Value = godowninfo.Narration;
            int inAffectedRows = sccmd.ExecuteNonQuery();
            if (inAffectedRows > 0)
            {
                isEdit = true;
            }
            else
            {
                isEdit = false;
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
       return isEdit;
    }
    /// <summary>
    /// Function to delete particular details based on the parameter by checking reference
    /// </summary>
    /// <param name="decGodownId"></param>
    /// <returns></returns>
    public decimal GodownCheckReferenceAndDelete(decimal decGodownId)
    {
        decimal decReturnValue = 0;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sqlcmd = new SqlCommand("GodownCheckReferenceAndDelete", Sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sqlcmd.Parameters.Add("@godownId", SqlDbType.Decimal);
            sprmparam.Value = decGodownId;
            decReturnValue = Convert.ToDecimal(sqlcmd.ExecuteNonQuery().ToString());
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return decReturnValue;
    }
    /// <summary>
    /// Function to get the godownId by godownname
    /// </summary>
    /// <param name="strGodown"></param>
    /// <returns></returns>
    public GodownInfo GodownIdByGodownName(string strGodown)
    {
        GodownInfo godowninfo = new GodownInfo();
        SqlDataReader sdrreader = null;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownView", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@godownName", SqlDbType.VarChar);
            sprmparam.Value = strGodown;
            sdrreader = sccmd.ExecuteReader();
            while (sdrreader.Read())
            {
                godowninfo.GodownId = Convert.ToDecimal(sdrreader[0].ToString());
                
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            sdrreader.Close();
            Sqlcon.Close();
        }
        return godowninfo;
    }
    /// <summary>
    /// Function to get default godownId by productName
    /// </summary>
    /// <param name="productName"></param>
    /// <returns></returns>
    public decimal DefaultGodownIDViewByProductName(string productName)
    {
        decimal godownId = 0;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("GodownIdViewByProductName", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@productName", SqlDbType.VarChar);
            sprmparam.Value = productName;
            godownId = Convert.ToDecimal(sccmd.ExecuteScalar());
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return godownId;
    }
  
}
}
