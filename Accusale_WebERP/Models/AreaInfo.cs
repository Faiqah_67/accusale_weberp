
using System;    

namespace AccuSale_Final.Models  
{    
public class AreaInfo    
{    
    public decimal _areaId;    
    public string _areaName;    
    public string _narration;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1 = string.Empty;    
    public string Extra2=string.Empty;    
    
    public decimal AreaId    
    {    
        get { return _areaId; }    
        set { _areaId = value; }    
    }    
    public string AreaName    
    {    
        get { return _areaName; }    
        set { _areaName = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
  
   
    
}    
}
