﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class UnitController : Controller
    {
        UnitSP spUnit = new UnitSP();

        // GET: Unit
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddUnit(UnitInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.UnitId == 0)
            {
                if (spUnit.UnitCheckExistence(datainfo.UnitName, 0) == false)
                {
                    spUnit.UnitAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.UnitId != 0)
            {
                spUnit.UnitEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteUnit(int UnitId)
        {
            spUnit.UnitDelete(UnitId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spUnit.UnitViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int UnitId)
        {
           
            UnitInfo datainfo = spUnit.UnitView(UnitId);

            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
       
    }
}