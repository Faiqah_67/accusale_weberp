
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;
   
namespace Accusale.WindowsApp
{
    class CounterSP 
    {
        #region Functions
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void CounterAdd(CounterInfo counterinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterName", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.CounterName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in Counter Table
        /// </summary>
        /// <param name="counterinfo"></param>
        public void CounterEdit(CounterInfo counterinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterId", SqlDbType.Decimal);
                sprmparam.Value = counterinfo.CounterId;
                sprmparam = sccmd.Parameters.Add("@counterName", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.CounterName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from Counter Table
        /// </summary>
        /// <returns></returns>
        public DataTable CounterViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("CounterViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from Counter table based on the parameter
        /// </summary>
        /// <param name="counterId"></param>
        /// <returns></returns>
        public CounterInfo CounterView(decimal counterId)
        {
            CounterInfo counterinfo = new CounterInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterId", SqlDbType.Decimal);
                sprmparam.Value = counterId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    counterinfo.CounterId = Convert.ToDecimal(sdrreader[0].ToString());
                    counterinfo.CounterName = sdrreader[1].ToString();
                    counterinfo.Narration = sdrreader[2].ToString();
                    counterinfo.ExtraDate = Convert.ToDateTime(sdrreader[3].ToString());
                    counterinfo.Extra1 = sdrreader[4].ToString();
                    counterinfo.Extra2 = sdrreader[5].ToString();
                }
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return counterinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="CounterId"></param>
        public void CounterDelete(decimal CounterId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterId", SqlDbType.Decimal);
                sprmparam.Value = CounterId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for Counter table
        /// </summary>
        /// <returns></returns>
        public int CounterGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = Convert.ToInt32(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to check the existance of a counter
        /// </summary>
        /// <param name="strCounterName"></param>
        /// <param name="strCounterId"></param>
        /// <returns></returns>
        public bool CounterCheckIfExist(String strCounterName, decimal strCounterId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("CounterCheckIfExist", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@counterName", SqlDbType.VarChar);
                sprmparam.Value = strCounterName;
                sprmparam = sqlcmd.Parameters.Add("@counterId", SqlDbType.Decimal);
                sprmparam.Value = strCounterId;
                object obj = sqlcmd.ExecuteScalar();
                decimal decCount = 0;
                if (obj != null)
                {
                    decCount = Convert.ToDecimal(obj.ToString());
                }
                if (decCount > 0)
                {
                    return true;
                }
                else
                {
                    return false; ;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return false;
        }
        /// <summary>
        /// Function to insert values to Counter Table
        /// </summary>
        /// <param name="counterinfo"></param>
        /// <returns></returns>
        public bool CounterAddSpecificFeilds(CounterInfo counterinfo)
        {
            bool isSave = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterAddSpecificFeilds", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterName", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.CounterName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Extra2;
                int inWork = sccmd.ExecuteNonQuery();
                if (inWork > 0)
                {
                    isSave = true;
                }
                else
                {
                    isSave = false;
                }
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isSave;
        }
        /// <summary>
        /// Function to Update values in Counter Table
        /// </summary>
        /// <param name="counterinfo"></param>
        /// <returns></returns>
        public bool CounterEditParticularField(CounterInfo counterinfo)
        {
            bool isEdit = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("counterEditParticularField", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterId", SqlDbType.Decimal);
                sprmparam.Value = counterinfo.CounterId;
                sprmparam = sccmd.Parameters.Add("@counterName", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.CounterName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = counterinfo.Narration;
                int inAffectedRows = sccmd.ExecuteNonQuery();
                if (inAffectedRows > 0)
                {
                    isEdit = true;
                }
                else
                {
                    isEdit = false;
                }
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return isEdit;
        }
        /// <summary>
        /// Function to view all counter
        /// </summary>
        /// <returns></returns>
        public DataTable CounterOnlyViewAll()
        {
            DataTable dtbl = new DataTable();
            dtbl.Columns.Add("SL.NO", typeof(decimal));
            dtbl.Columns["SL.NO"].AutoIncrement = true;
            dtbl.Columns["SL.NO"].AutoIncrementSeed = 1;
            dtbl.Columns["SL.NO"].AutoIncrementStep = 1;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("CounterOnlyViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Counter view with narration 
        /// </summary>
        /// <param name="decCounterId"></param>
        /// <returns></returns>
        public CounterInfo CounterWithNarrationView(decimal decCounterId)
        {
            CounterInfo counterinfo = new CounterInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterWithNarrationView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterId", SqlDbType.Decimal);
                sprmparam.Value = decCounterId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    counterinfo.CounterId = Convert.ToDecimal(sdrreader[0].ToString());
                    counterinfo.CounterName = sdrreader[1].ToString();
                    counterinfo.Narration = sdrreader[2].ToString();
                }
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return counterinfo;
        }
        /// <summary>
        /// Counter check reference and delete 
        /// </summary>
        /// <param name="decCounterId"></param>
        /// <returns></returns>
        public decimal CounterCheckReferenceAndDelete(decimal decCounterId)
        {
            decimal decReturnValue = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("CounterCheckReferenceAndDelete", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@counterId", SqlDbType.Decimal);
                sprmparam.Value = decCounterId;
                decReturnValue = Convert.ToDecimal(sqlcmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decReturnValue;
        }
        /// <summary>
        /// Function to insert values to Counter Table
        /// </summary>
        /// <param name="CounterInfo"></param>
        /// <returns></returns>
        public decimal CounterAddWithIdentity(CounterInfo CounterInfo)
        {
            decimal decLedgerId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CounterAddWithIdentity", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@counterName", SqlDbType.VarChar);
                sprmparam.Value = CounterInfo.CounterName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = CounterInfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = CounterInfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = CounterInfo.Extra2;
                object obj = sccmd.ExecuteScalar();
                if (obj != null)
                {
                    decLedgerId = Convert.ToDecimal(obj.ToString());
                }
                else
                {
                    decLedgerId = 0;
                }
            }
            catch (Exception ex)
            {
              ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decLedgerId;
        }
        #endregion
    }
}
