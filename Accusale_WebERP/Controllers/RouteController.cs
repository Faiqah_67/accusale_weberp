﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;

namespace Accusale_WebERP.Controllers
{
    public class RouteController : Controller
    {
        RouteSP spRoute = new RouteSP();
        // GET: Route
        public ActionResult Index()
        {
            return View();
        }
                public JsonResult AddRoute(RouteInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.RouteId == 0)
            {
                if (spRoute.RouteCheckExistence(datainfo.RouteName, 0,0) == false)
                {
                    spRoute.RouteAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.RouteId != 0)
            {
                spRoute.RouteEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteRoute(int RouteId)
        {
            spRoute.RouteDelete(RouteId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spRoute.RouteViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int RouteId)
        {
            RouteInfo datainfo = spRoute.RouteView(RouteId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}