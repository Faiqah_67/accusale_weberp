﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class RackController : Controller
    {
        RackSP spRack = new RackSP();
        // GET: Rack
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddRack(RackInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.RackId == 0)
            {
                if (spRack.RackCheckExistence(datainfo.RackName, 0,datainfo.GodownId) == false)
                {
                    spRack.RackAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.RackId != 0)
            {
                spRack.RackEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteRack(int RackId)
        {
            if (spRack.RackDeleteReference(RackId) <= 0)
            {
                return Json(new { message = "You can't delete,reference exist" });
                
            }
            else
            {
                spRack.RackDelete(RackId);
                return Json(new { message = "Data delete successfully" });

            }

        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spRack.RackViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int RackId)
        {
            RackInfo datainfo = spRack.RackView(RackId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}