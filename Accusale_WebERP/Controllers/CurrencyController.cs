﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class CurrencyController : Controller
    {
        CurrencySP spCurrency = new CurrencySP();

        // GET: Currency
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddCurrency(CurrencyInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.CurrencyId == 0)
            {
                if (spCurrency.CurrencyNameCheckExistence(datainfo.CurrencyName, datainfo.CurrencySymbol, 0) == false)
                {
                    spCurrency.CurrencyAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.CurrencyId != 0)
            {
                spCurrency.CurrencyEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteCurrency(int CurrencyId)
        {
            spCurrency.CurrencyDelete(CurrencyId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spCurrency.CurrencyViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int CurrencyId)
        {
 
            CurrencyInfo datainfo = spCurrency.CurrencyView(CurrencyId);

            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    
    }
}