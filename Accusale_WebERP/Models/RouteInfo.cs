
using System;    

namespace AccuSale_Final.Models
{    
public class RouteInfo    
{    
    public decimal _routeId;    
    public string _routeName;    
    public decimal _areaId;    
    public string _narration;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2=string.Empty;    
    
    public decimal RouteId    
    {    
        get { return _routeId; }    
        set { _routeId = value; }    
    }    
    public string RouteName    
    {    
        get { return _routeName; }    
        set { _routeName = value; }    
    }    
    public decimal AreaId    
    {    
        get { return _areaId; }    
        set { _areaId = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
  
    
}    
}
