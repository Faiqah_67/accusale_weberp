using System;      
namespace AccuSale_Final.Models
{    
public class GodownInfo    
{    
    public decimal _godownId;    
    public string _godownName;    
    public string _narration;    
    public DateTime ExtraDate = DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2 = string.Empty;    
    
    public decimal GodownId    
    {    
        get { return _godownId; }    
        set { _godownId = value; }    
    }    
    public string GodownName    
    {    
        get { return _godownName; }    
        set { _godownName = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
     
    
}    
}
