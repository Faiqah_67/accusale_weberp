
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;
   
namespace AccuSale_Final.DAL
{
    class LedgerPostingSP 
    {
        #region Function
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void LedgerPostingAdd(LedgerPostingInfo ledgerpostinginfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = ledgerpostinginfo.Date;
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.VoucherTypeId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.VoucherNo;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.LedgerId;
                sprmparam = sccmd.Parameters.Add("@debit", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.Debit;
                sprmparam = sccmd.Parameters.Add("@credit", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.Credit;
                sprmparam = sccmd.Parameters.Add("@detailsId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.DetailsId;
                sprmparam = sccmd.Parameters.Add("@yearId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.YearId;
                sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.InvoiceNo;
                sprmparam = sccmd.Parameters.Add("@chequeNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.ChequeNo;
                sprmparam = sccmd.Parameters.Add("@chequeDate", SqlDbType.DateTime);
                sprmparam.Value = ledgerpostinginfo.ChequeDate;
                
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
       /// <summary>
        /// Function to Update values in LedgerPosting Table
       /// </summary>
       /// <param name="ledgerpostinginfo"></param>
        public void LedgerPostingEdit(LedgerPostingInfo ledgerpostinginfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@ledgerPostingId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.LedgerPostingId;
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = ledgerpostinginfo.Date;
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.VoucherTypeId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.VoucherNo;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.LedgerId;
                sprmparam = sccmd.Parameters.Add("@debit", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.Debit;
                sprmparam = sccmd.Parameters.Add("@credit", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.Credit;
                sprmparam = sccmd.Parameters.Add("@detailsId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.DetailsId;
                sprmparam = sccmd.Parameters.Add("@yearId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.YearId;
                sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.InvoiceNo;
                sprmparam = sccmd.Parameters.Add("@chequeNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.ChequeNo;
                sprmparam = sccmd.Parameters.Add("@chequeDate", SqlDbType.DateTime);
                sprmparam.Value = ledgerpostinginfo.ChequeDate;
               
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from LedgerPosting Table
        /// </summary>
        /// <returns></returns>
        public DataTable LedgerPostingViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("LedgerPostingViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        
        /// <summary>
        /// Function to get particular values from LedgerPosting table based on the parameter
        /// </summary>
        /// <param name="ledgerPostingId"></param>
        /// <returns></returns>
        public LedgerPostingInfo LedgerPostingView(decimal ledgerPostingId)
        {
            LedgerPostingInfo ledgerpostinginfo = new LedgerPostingInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@ledgerPostingId", SqlDbType.Decimal);
                sprmparam.Value = ledgerPostingId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    ledgerpostinginfo.LedgerPostingId = decimal.Parse(sdrreader[0].ToString());
                    ledgerpostinginfo.Date = DateTime.Parse(sdrreader[1].ToString());
                    ledgerpostinginfo.VoucherTypeId = decimal.Parse(sdrreader[2].ToString());
                    ledgerpostinginfo.VoucherNo = sdrreader[3].ToString();
                    ledgerpostinginfo.LedgerId = decimal.Parse(sdrreader[4].ToString());
                    ledgerpostinginfo.Debit = decimal.Parse(sdrreader[5].ToString());
                    ledgerpostinginfo.Credit = decimal.Parse(sdrreader[6].ToString());
                    ledgerpostinginfo.DetailsId = decimal.Parse(sdrreader[8].ToString());
                    ledgerpostinginfo.YearId = decimal.Parse(sdrreader[9].ToString());
                    ledgerpostinginfo.InvoiceNo = sdrreader[10].ToString();
                    ledgerpostinginfo.ChequeNo = sdrreader[11].ToString();
                    ledgerpostinginfo.ChequeDate = DateTime.Parse(sdrreader[12].ToString());
                    ledgerpostinginfo.ExtraDate = DateTime.Parse(sdrreader[13].ToString());
                    ledgerpostinginfo.Extra1 = sdrreader[14].ToString();
                    ledgerpostinginfo.Extra2 = sdrreader[15].ToString();
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return ledgerpostinginfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="LedgerPostingId"></param>
        public void LedgerPostingDelete(decimal LedgerPostingId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@ledgerPostingId", SqlDbType.Decimal);
                sprmparam.Value = LedgerPostingId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for LedgerPosting table
        /// </summary>
        /// <returns></returns>
        public int LedgerPostingGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="strVoucherNo"></param>
        /// <param name="decVoucherTypeId"></param>
        public void LedgerPostDelete(string strVoucherNo, decimal decVoucherTypeId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = decVoucherTypeId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = strVoucherNo;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get ledgerpostingIds
        /// </summary>
        /// <param name="voucherNo"></param>
        /// <param name="voucherTypeId"></param>
        /// <returns></returns>
        public DataTable GetLedgerPostingIds(string voucherNo, decimal voucherTypeId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("GetLedgerPostingIds", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.SelectCommand.Parameters.Add("@voucherNo", SqlDbType.VarChar).Value = voucherNo;
                sdaadapter.SelectCommand.Parameters.Add("@voucherTypeId", SqlDbType.Decimal).Value = voucherTypeId;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to Update values in LedgerPosting Table by voucherNo and voucherTypeId
        /// </summary>
        /// <param name="ledgerpostinginfo"></param>
        public void LedgerPostingEditByVoucherTypeAndVoucherNo(LedgerPostingInfo ledgerpostinginfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingEditByVoucherTypeAndVoucherNo", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = ledgerpostinginfo.Date;
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.VoucherTypeId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.VoucherNo;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.LedgerId;
                sprmparam = sccmd.Parameters.Add("@debit", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.Debit;
                sprmparam = sccmd.Parameters.Add("@credit", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.Credit;
                sprmparam = sccmd.Parameters.Add("@detailsId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.DetailsId;
                sprmparam = sccmd.Parameters.Add("@yearId", SqlDbType.Decimal);
                sprmparam.Value = ledgerpostinginfo.YearId;
                sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.InvoiceNo;
                sprmparam = sccmd.Parameters.Add("@chequeNo", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.ChequeNo;
                sprmparam = sccmd.Parameters.Add("@chequeDate", SqlDbType.DateTime);
                sprmparam.Value = ledgerpostinginfo.ChequeDate;
                
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = ledgerpostinginfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
       
        /// <summary>
        /// get ledgerposting id by detailsId
        /// </summary>
        /// <param name="decDetailsId"></param>
        /// <param name="strVoucherNo"></param>
        /// <param name="decVoucherTypeId"></param>
        /// <returns></returns>
        public decimal LedgerPostingIdFromDetailsId(decimal decDetailsId, string strVoucherNo, decimal decVoucherTypeId)
        {
            decimal decLedgerPostingId = 0;
            SqlDataReader sqldr = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingIdFromDetailsId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@detailsId", SqlDbType.Decimal);
                sprmparam.Value = decDetailsId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = strVoucherNo;
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = decVoucherTypeId;
                sqldr = sccmd.ExecuteReader();
                while (sqldr.Read())
                {
                    decLedgerPostingId = Convert.ToDecimal(sqldr["ledgerPostingId"].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decLedgerPostingId;
        }
       /// <summary>
       /// Function to delete ledgerPosting by detailsId
       /// </summary>
       /// <param name="decDetailsId"></param>
       /// <param name="strVoucherNo"></param>
       /// <param name="decVoucherTypeId"></param>
        public void LedgerPostDeleteByDetailsId(decimal decDetailsId, string strVoucherNo, decimal decVoucherTypeId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostDeleteByDetailsId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@DetailsId", SqlDbType.Decimal);
                sprmparam.Value = decDetailsId;
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = decVoucherTypeId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = strVoucherNo;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get ledgerPostingId by voucherNo and voucherTypeId
        /// </summary>
        /// <param name="strVoucherNo"></param>
        /// <param name="decVoucherTypeId"></param>
        /// <returns></returns>
        public decimal LedgerPostingIdForTotalAmount(string strVoucherNo, decimal decVoucherTypeId)
        {
            decimal decLedgerPostingId = 0;
            SqlDataReader sqldr = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingIdForTotalAmount", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = strVoucherNo;
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = decVoucherTypeId;
                sqldr = sccmd.ExecuteReader();
                while (sqldr.Read())
                {
                    decLedgerPostingId = Convert.ToDecimal(sqldr["ledgerPostingId"].ToString());
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decLedgerPostingId;
        }
        
        /// <summary>
        /// Ledgerposting and partybalance delete based on parameters
        /// </summary>
        /// <param name="voucherTypeId"></param>
        /// <param name="voucherNo"></param>
        /// <param name="invoiceNo"></param>
        public void LedgerPostingAndPartyBalanceDeleteByVoucherTypeIdAndLedgerIdAndVoucherNo(decimal voucherTypeId, string voucherNo, string invoiceNo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingAndPartyBalanceDeleteByVoucherTypeIdAndVoucherNoAndInvoiceNo", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = voucherTypeId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = voucherNo;
                sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                sprmparam.Value = invoiceNo;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
      
        /// <summary>
        /// ledgerPosting edit by vouchertype and voucherNo and ledgerId
        /// </summary>
        /// <param name="infoLedgerPosting"></param>
        public void LedgerPostingEditByVoucherTypeAndVoucherNoAndLedgerId(LedgerPostingInfo infoLedgerPosting)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingEditByVoucherTypeAndVoucherNoAndLedgerId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = infoLedgerPosting.Date;
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = infoLedgerPosting.VoucherTypeId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = infoLedgerPosting.VoucherNo;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = infoLedgerPosting.LedgerId;
                sprmparam = sccmd.Parameters.Add("@debit", SqlDbType.Decimal);
                sprmparam.Value = infoLedgerPosting.Debit;
                sprmparam = sccmd.Parameters.Add("@credit", SqlDbType.Decimal);
                sprmparam.Value = infoLedgerPosting.Credit;
                sprmparam = sccmd.Parameters.Add("@detailsId", SqlDbType.Decimal);
                sprmparam.Value = infoLedgerPosting.DetailsId;
                sprmparam = sccmd.Parameters.Add("@yearId", SqlDbType.Decimal);
                sprmparam.Value = infoLedgerPosting.YearId;
                sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                sprmparam.Value = infoLedgerPosting.InvoiceNo;
                sprmparam = sccmd.Parameters.Add("@chequeNo", SqlDbType.VarChar);
                sprmparam.Value = infoLedgerPosting.ChequeNo;
                sprmparam = sccmd.Parameters.Add("@chequeDate", SqlDbType.DateTime);
                sprmparam.Value = infoLedgerPosting.ChequeDate;
                
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = infoLedgerPosting.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = infoLedgerPosting.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        
        /// <summary>
        /// ledgerPosting edit by vouchertype and voucherNo and ledgerId and extra
        /// </summary>
        /// <param name="voucherTypeId"></param>
        /// <param name="decLedgerId"></param>
        /// <param name="strVoucherNo"></param>
        /// <param name="strAddCash"></param>
        public void LedgerPostingDeleteByVoucherTypeIdAndLedgerIdAndVoucherNoAndExtra(decimal voucherTypeId, decimal decLedgerId, string strVoucherNo, string strAddCash)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("LedgerPostingDeleteByVoucherTypeIdAndLedgerIdAndVoucherNoAndExtra", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sprmparam.Value = voucherTypeId;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = decLedgerId;
                sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                sprmparam.Value = strVoucherNo;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = strAddCash;
                
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Delete ledgerposting for stock journal edit
        /// </summary>
        /// <param name="strVoucherNo"></param>
        /// <param name="decVoucherTypeId"></param>
        public void DeleteLedgerPostingForStockJournalEdit(string strVoucherNo, decimal decVoucherTypeId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("DeleteLedgerPostingForStockJournalEdit", Sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@voucherNo", SqlDbType.VarChar).Value = strVoucherNo;
                cmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal).Value = decVoucherTypeId;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }

        /// <summary>
        /// Delete ledgerposting for Forex Currency conversion
        /// </summary>
        /// <param name="strVoucherNo"></param>
        /// <param name="decVoucherTypeId"></param>
        public void LedgerPostingDeleteByVoucherNoVoucherTypeIdAndLedgerId(string strVoucherNo, decimal decVoucherTypeId, decimal decLedgerId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("LedgerPostingDeleteByVoucherNoVoucherTypeIdAndLedgerId", Sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@voucherNo", SqlDbType.VarChar).Value = strVoucherNo;
                cmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal).Value = decVoucherTypeId;
                cmd.Parameters.Add("@ledgerId", SqlDbType.Decimal).Value = decLedgerId;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        #endregion
    }
}
