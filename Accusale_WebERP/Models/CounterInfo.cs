
using System;    
   
namespace AccuSale_Final.Models   
{    
public class CounterInfo    
{    
    public decimal _counterId;    
    public string _counterName;    
    public string _narration;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2 = string.Empty;    
    
    public decimal CounterId    
    {    
        get { return _counterId; }    
        set { _counterId = value; }    
    }    
    public string CounterName    
    {    
        get { return _counterName; }    
        set { _counterName = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
  
    
}    
}
