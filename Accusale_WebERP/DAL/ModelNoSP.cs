
using AccuSale_Final.Models;
using System;    
using System.Data;
using System.Data.SqlClient;
    
  
namespace Accusale_Final.DAL
{    
class ModelNoSP
{
        #region Function
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public decimal ModelNoAddWithDifferentModelNo(ModelNoInfo modelnoinfo)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("ModelNoAddWithDifferentModelNo", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@modelNo", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.ModelNo;
            sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.Narration;
            sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.Extra1;
            sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.Extra2;
            decimal decWork=Convert.ToDecimal( sccmd.ExecuteScalar());
            if (decWork > 0)
            {
                return decWork;   
            }
            else
            {
                return 0;
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
            return 0;
        }
        finally
        {
            Sqlcon.Close();
        }
       
    }
    /// <summary>
    /// Function to Update values in ModelNo Table
    /// </summary>
    /// <param name="modelnoinfo"></param>
    public void ModelNoEdit(ModelNoInfo modelnoinfo)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("ModelNoEdit", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@modelNoId", SqlDbType.Decimal);
            sprmparam.Value = modelnoinfo.ModelNoId;
            sprmparam = sccmd.Parameters.Add("@modelNo", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.ModelNo;
            sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.Narration;
            sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.Extra1;
            sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.Extra2;
            sccmd.ExecuteNonQuery();        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
    }
   
    /// <summary>
    /// Function to get all the values from ModelNo Table
    /// </summary>
    /// <returns></returns>
    public DataTable ModelNoViewAll()
    {
        DataTable dtbl = new DataTable();
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlDataAdapter sdaadapter = new SqlDataAdapter("ModelNoViewAll", Sqlcon);
            sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaadapter.Fill(dtbl);
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return dtbl;
    }
    /// <summary>
    /// Function to get particular values from ModelNo table based on the parameter
    /// </summary>
    /// <param name="modelNoId"></param>
    /// <returns></returns>
    public ModelNoInfo ModelNoView(decimal modelNoId )
    {
        ModelNoInfo modelnoinfo =new ModelNoInfo();
        SqlDataReader sdrreader =null;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("ModelNoView", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@modelNoId", SqlDbType.Decimal);
            sprmparam.Value = modelNoId;
             sdrreader = sccmd.ExecuteReader();
            while (sdrreader.Read())
            {
                modelnoinfo.ModelNoId = Convert.ToDecimal(sdrreader[0].ToString());
                modelnoinfo.ModelNo= sdrreader[1].ToString();
                modelnoinfo.Narration= sdrreader[2].ToString();
                modelnoinfo.ExtraDate=Convert.ToDateTime(sdrreader[3].ToString());
                modelnoinfo.Extra1= sdrreader[4].ToString();
                modelnoinfo.Extra2= sdrreader[5].ToString();
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {           sdrreader.Close(); 
            Sqlcon.Close();
        }
        return modelnoinfo;
    }
    /// <summary>
    /// Function to delete particular details based on the parameter
    /// </summary>
    /// <param name="ModelNoId"></param>
    public void ModelNoDelete(decimal ModelNoId)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("ModelNoDelete", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@modelNoId", SqlDbType.Decimal);
            sprmparam.Value = ModelNoId;
            sccmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
    }
    /// <summary>
    /// Function to  get the next id for ModelNo table
    /// </summary>
    /// <returns></returns>
    public int ModelNoGetMax()
    {
        int max = 0;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("ModelNoMax", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            max = Convert.ToInt32(sccmd.ExecuteScalar().ToString());
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return max;
    }
   /// <summary>
    /// Function to get all the values from ModelNo Table
   /// </summary>
   /// <returns></returns>
    public DataTable ModelNoOnlyViewAll()
    {
        DataTable dtbl = new DataTable();
        dtbl.Columns.Add("SL.NO", typeof(decimal));
        dtbl.Columns["SL.NO"].AutoIncrement = true;
        dtbl.Columns["SL.NO"].AutoIncrementSeed = 1;
        dtbl.Columns["SL.NO"].AutoIncrementStep = 1;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlDataAdapter sdaadapter = new SqlDataAdapter("ModelNoOnlyViewAll", Sqlcon);
            sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            sdaadapter.Fill(dtbl);
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return dtbl;
    }
    /// <summary>
    /// Function to get particular values from ModelNo table based on the parameter with narration
    /// </summary>
    /// <param name="decModelNoId"></param>
    /// <returns></returns>
    public ModelNoInfo ModelNoWithNarrationView(decimal decModelNoId)
    {
        ModelNoInfo modelnoinfo = new ModelNoInfo();
        SqlDataReader sdrreader = null;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("ModelNoWithNarrationView", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@modelNoId", SqlDbType.Decimal);
            sprmparam.Value = decModelNoId;
            sdrreader = sccmd.ExecuteReader();
            while (sdrreader.Read())
            {
                modelnoinfo.ModelNoId = Convert.ToDecimal(sdrreader[0].ToString());
                modelnoinfo.ModelNo = sdrreader[1].ToString();
                modelnoinfo.Narration = sdrreader[2].ToString();    
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            sdrreader.Close();
            Sqlcon.Close();
        }
        return modelnoinfo;
    }
    /// <summary>
    /// Function to check existance of modelNo
    /// </summary>
    /// <param name="strModelNo"></param>
    /// <param name="decModelNoId"></param>
    /// <returns></returns>
    public bool ModelCheckIfExist(String strModelNo, decimal decModelNoId)
    {
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sqlcmd = new SqlCommand("ModelCheckIfExist", Sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sqlcmd.Parameters.Add("@modelNo", SqlDbType.VarChar);
            sprmparam.Value = strModelNo;
            sprmparam = sqlcmd.Parameters.Add("@modelNoId", SqlDbType.Decimal);
            sprmparam.Value = decModelNoId;
            object obj = sqlcmd.ExecuteScalar();
            decimal decCount = 0;
            if (obj != null)
            {
                decCount = Convert.ToDecimal(obj.ToString());
            }
            if (decCount > 0)
            {
                return true;
            }
            else
            {
                return false; ;
            }
        }
        catch (Exception ex)
        {
                ex.ToString();
            }
        finally
        {
            Sqlcon.Close();
        }
        return false;
    }
    /// <summary>
    /// Function to Update values in ModelNo Table
    /// </summary>
    /// <param name="modelnoinfo"></param>
    /// <returns></returns>
    public bool ModelNoEditParticularFeilds(ModelNoInfo modelnoinfo)
    {
        bool isEdit = false;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sccmd = new SqlCommand("ModelNoEditParticularFeild", Sqlcon);
            sccmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sccmd.Parameters.Add("@modelNoId", SqlDbType.Decimal);
            sprmparam.Value = modelnoinfo.ModelNoId;
            sprmparam = sccmd.Parameters.Add("@modelNo", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.ModelNo;
            sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
            sprmparam.Value = modelnoinfo.Narration;
            int inAffectedRows = sccmd.ExecuteNonQuery();
            if (inAffectedRows > 0)
            {
                isEdit = true;
            }
            else
            {
                isEdit = false;
            }
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return isEdit;
    }
    /// <summary>
    /// Function to delete particular details based on the parameter by checking reference
    /// </summary>
    /// <param name="decModelNoId"></param>
    /// <returns></returns>
    public decimal ModelNoCheckReferenceAndDelete(decimal decModelNoId)
    {
        decimal decReturnValue = 0;
        try
        {
            if (Sqlcon.State == ConnectionState.Closed)
            {
                Sqlcon.Open();
            }
            SqlCommand sqlcmd = new SqlCommand("ModelNoCheckReferenceAndDelete", Sqlcon);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            SqlParameter sprmparam = new SqlParameter();
            sprmparam = sqlcmd.Parameters.Add("@modelNoId", SqlDbType.Decimal);
            sprmparam.Value = decModelNoId;
            decReturnValue = Convert.ToDecimal(sqlcmd.ExecuteNonQuery().ToString());
        }
        catch (Exception ex)
        {
           ex.ToString();
        }
        finally
        {
            Sqlcon.Close();
        }
        return decReturnValue;
    }
    #endregion
}
}
