using System;    
   
namespace AccuSale_Final.Models
{    
public class RackInfo    
{    
    public decimal _rackId;    
    public string _rackName;    
    public decimal _godownId;    
    public string _narration;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2=string.Empty;    
    
    public decimal RackId    
    {    
        get { return _rackId; }    
        set { _rackId = value; }    
    }    
    public string RackName    
    {    
        get { return _rackName; }    
        set { _rackName = value; }    
    }    
    public decimal GodownId    
    {    
        get { return _godownId; }    
        set { _godownId = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
   
    
}    
}
