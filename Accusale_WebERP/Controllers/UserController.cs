﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class UserController : Controller
    {
        UserSP spUser = new UserSP();

        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult UserAdd(UserInfo userinfo)
        {
            var outputmsg = string.Empty;


            if (userinfo.UserId == 0)
            {
                if (spUser.UserCreationCheckExistence(userinfo.UserId, userinfo.UserName) == false)
                {
                    spUser.UserAdd(userinfo);
                    outputmsg = "User created successfully";
                }
                else
                {
                    outputmsg = "User already exists";

                }
            }


            if (userinfo.UserId != 0)
            {
                spUser.UserEdit(userinfo); outputmsg = "User updated successfully";
            }


            return Json(new { message = outputmsg });
            //try { 
            //if (userinfo.UserId == 0) { spUser.UserAdd(userinfo); }
            //else { spUser.UserEdit(userinfo); }
            //}
            //catch (Exception ex)
            //{
            //    return Json(new { Error = ex.ToString() });
            //}

            //return Json(1);
        }

        [HttpPost]
        public JsonResult UserDelete(int UserId)
        {
            spUser.UserDelete(UserId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowUser()
        {
            DataTable dt = spUser.UserViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
    }
}