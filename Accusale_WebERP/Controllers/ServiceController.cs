﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class ServiceController : Controller
    {
        ServiceSP spService = new ServiceSP();
        // GET: Service
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddService(ServiceInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.ServiceId == 0)
            {
                if (spService.ServiceCheckExistence(datainfo.ServiceName, 0) == false)
                {
                    spService.ServiceAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.ServiceId != 0)
            {
                spService.ServiceEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteService(int ServiceId)
        {
            spService.ServiceDelete(ServiceId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spService.ServiceViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int ServiceId)
        {

            ServiceInfo datainfo = spService.ServiceView(ServiceId);

            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
      
    }
}