
using System;

namespace AccuSale_Final.Models 
{    
public class SizeInfo    
{    
    public decimal _sizeId;    
    public string _size;    
    public string _narration;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1 = string.Empty;    
    public string Extra2= string.Empty;      
    
    public decimal SizeId    
    {    
        get { return _sizeId; }    
        set { _sizeId = value; }    
    }    
    public string Size    
    {    
        get { return _size; }    
        set { _size = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
  
    
}    
}
