﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class PriceListController : Controller
    {
        PriceListSP PriceListSP = new PriceListSP();
        // GET: PriceList
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult ShowData(string groupId, string strProductCode, string strProductName, string strPricingLevelName)
        {
            decimal decgroupId = Convert.ToDecimal(groupId);
            DataTable dt = PriceListSP.ProductDetailsViewGridfill( decgroupId,  strProductCode,  strProductName,  strPricingLevelName);
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ShowProductGroup()
        {
            DataTable dt = PriceListSP.PricelistProductGroupViewAllForComboBox();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ShowPricingLevel()
        {
            DataTable dt = PriceListSP.PricelistPricingLevelViewAllForComboBox();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
    }
}