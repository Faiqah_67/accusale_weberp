﻿using Accusale.WindowsApp;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class CounterController : Controller
    {
        CounterSP spCounter = new CounterSP();
        // GET: Counter
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddCounter(CounterInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.CounterId == 0)
            {
                if (spCounter.CounterCheckIfExist(datainfo.CounterName, 0) == false)
                {
                    spCounter.CounterAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.CounterId != 0)
            {
                spCounter.CounterEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteCounter(int CounterId)
        {
            spCounter.CounterDelete(CounterId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spCounter.CounterViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int CounterId)
        {
            CounterInfo datainfo = spCounter.CounterView(CounterId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}