
using System;    
   
namespace AccuSale_Final.Models  
{    
public class EmployeeInfo    
{    
    public decimal _employeeId;    
    public decimal _designationId;    
    public string _employeeName;    
    public string _employeeCode;    
    public DateTime _dob;    
    public string _maritalStatus;    
    public string _gender;    
    public string _qualification;    
    public string _address;    
    public string _phoneNumber;    
    public string _mobileNumber;    
    public string _email;
    public DateTime _joiningDate;
    public DateTime _terminationDate;    
    public bool _isActive;    
    public string _narration;    
    public string _bloodGroup;    
    public string _passportNo;    
    public DateTime _passportExpiryDate;    
    public string _labourCardNumber;    
    public DateTime _labourCardExpiryDate;    
    public string _visaNumber;    
    public DateTime _visaExpiryDate;    
    public string _salaryType;    
    public decimal _dailyWage;
    public string _bankName;
    public string _branchName;
    public string _bankAccountNumber;
    public string _branchCode;
    public string _panNumber;
    public string _pfNumber;
    public string _esiNumber;
    public DateTime ExtraDate=DateTime.Now;
    public string Extra1=string.Empty;
    public string Extra2=string.Empty;
    public decimal _defaultPackageId;


    public decimal DefaultPackageId
    {
        get { return _defaultPackageId; }
        set{ _defaultPackageId = value;}
        
    }
    
    public decimal EmployeeId    
    {    
        get { return _employeeId; }    
        set { _employeeId = value; }    
    }    
    public decimal DesignationId    
    {    
        get { return _designationId; }    
        set { _designationId = value; }    
    }    
    public string EmployeeName    
    {    
        get { return _employeeName; }    
        set { _employeeName = value; }    
    }    
    public string EmployeeCode    
    {    
        get { return _employeeCode; }    
        set { _employeeCode = value; }    
    }    
    public DateTime Dob    
    {    
        get { return _dob; }    
        set { _dob = value; }    
    }    
    public string MaritalStatus    
    {    
        get { return _maritalStatus; }    
        set { _maritalStatus = value; }    
    }    
    public string Gender    
    {    
        get { return _gender; }    
        set { _gender = value; }    
    }    
    public string Qualification    
    {    
        get { return _qualification; }    
        set { _qualification = value; }    
    }    
    public string Address    
    {    
        get { return _address; }    
        set { _address = value; }    
    }    
    public string PhoneNumber    
    {    
        get { return _phoneNumber; }    
        set { _phoneNumber = value; }    
    }    
    public string MobileNumber    
    {    
        get { return _mobileNumber; }    
        set { _mobileNumber = value; }    
    }    
    public string Email    
    {    
        get { return _email; }    
        set { _email = value; }    
    }
    public DateTime JoiningDate
    {
        get { return _joiningDate; }
        set { _joiningDate = value; }
    }    
    public DateTime TerminationDate    
    {    
        get { return _terminationDate; }    
        set { _terminationDate = value; }    
    }    
    public bool IsActive    
    {    
        get { return _isActive; }    
        set { _isActive = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
    public string BloodGroup    
    {    
        get { return _bloodGroup; }    
        set { _bloodGroup = value; }    
    }    
    public string PassportNo    
    {    
        get { return _passportNo; }    
        set { _passportNo = value; }    
    }    
    public DateTime PassportExpiryDate    
    {    
        get { return _passportExpiryDate; }    
        set { _passportExpiryDate = value; }    
    }    
    public string LabourCardNumber    
    {    
        get { return _labourCardNumber; }    
        set { _labourCardNumber = value; }    
    }    
    public DateTime LabourCardExpiryDate    
    {    
        get { return _labourCardExpiryDate; }    
        set { _labourCardExpiryDate = value; }    
    }    
    public string VisaNumber    
    {    
        get { return _visaNumber; }    
        set { _visaNumber = value; }    
    }    
    public DateTime VisaExpiryDate    
    {    
        get { return _visaExpiryDate; }    
        set { _visaExpiryDate = value; }    
    }    
    public string SalaryType    
    {    
        get { return _salaryType; }    
        set { _salaryType = value; }    
    }    
    public decimal DailyWage    
    {    
        get { return _dailyWage; }    
        set { _dailyWage = value; }    
    }
    public string BankName
    {
        get { return _bankName; }
        set { _bankName = value; }
    }
    public string BranchName
    {
        get { return _branchName; }
        set { _branchName = value; }
    }
    public string BankAccountNumber
    {
        get { return _bankAccountNumber; }
        set { _bankAccountNumber = value; }
    }
    public string BranchCode
    {
        get { return _branchCode; }
        set { _branchCode = value; }
    }
    public string PanNumber
    {
        get { return _panNumber; }
        set { _panNumber = value; }
    }
    public string PfNumber
    {
        get { return _pfNumber; }
        set { _pfNumber = value; }
    }
    public string EsiNumber
    {
        get { return _esiNumber; }
        set { _esiNumber = value; }
    }
 
    
}    
}
