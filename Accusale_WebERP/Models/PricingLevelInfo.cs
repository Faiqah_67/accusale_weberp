
using System;    

namespace AccuSale_Final.Models
{    
public class PricingLevelInfo    
{    
    public decimal _pricinglevelId;    
    public string _pricinglevelName;    
    public string _narration;    
    public DateTime ExtraDate=DateTime.Now;    
    public string Extra1=string.Empty;    
    public string Extra2 = string.Empty;

        public decimal PricinglevelId    
    {    
        get { return _pricinglevelId; }    
        set { _pricinglevelId = value; }    
    }    
    public string PricinglevelName    
    {    
        get { return _pricinglevelName; }    
        set { _pricinglevelName = value; }    
    }    
    public string Narration    
    {    
        get { return _narration; }    
        set { _narration = value; }    
    }    
    
    
}    
}
