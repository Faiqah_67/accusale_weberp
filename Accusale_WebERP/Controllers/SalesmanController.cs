﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
   
    public class SalesmanController : Controller
    {
        EmployeeSP spEmployee = new EmployeeSP();
        // GET: Salesman
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddSalesman(EmployeeInfo datainfo)
        {
            var outputmsg = string.Empty;
            datainfo.SalaryType = "Monthly";
            datainfo.DefaultPackageId = 1;
            datainfo.BankName = string.Empty;
            datainfo.BankAccountNumber = string.Empty;
            datainfo.BranchName = string.Empty;
            datainfo.BranchCode = string.Empty;
            datainfo.PanNumber = string.Empty;
            datainfo.PfNumber = string.Empty;
            datainfo.EsiNumber = string.Empty;
            datainfo.PassportNo = string.Empty;
            datainfo.PassportExpiryDate = DateTime.Now;
            datainfo.VisaNumber = string.Empty;
            datainfo.VisaExpiryDate = DateTime.Now;
            datainfo.LabourCardNumber = string.Empty;
            datainfo.LabourCardExpiryDate = DateTime.Now;
            datainfo.Extra1 = string.Empty;
            datainfo.Extra2 = string.Empty;
            datainfo.Dob = DateTime.Now;
            datainfo.MaritalStatus = "Single";
            datainfo.Gender = "Male";
            datainfo.Qualification = string.Empty;
            datainfo.BloodGroup = string.Empty;
            datainfo.JoiningDate = DateTime.Now;
            datainfo.TerminationDate = DateTime.Now;

            if (datainfo.EmployeeId == 0)
            {
                if (spEmployee.EmployeeCodeCheckExistance(datainfo.EmployeeCode, 0) == false)
                {
                   

                    spEmployee.EmployeeAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.EmployeeId != 0)
            {
                spEmployee.EmployeeEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteSalesman(int EmployeeId)
        {
            spEmployee.EmployeeDelete(EmployeeId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spEmployee.EmployeeViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int EmployeeId)
        {

            EmployeeInfo datainfo = spEmployee.EmployeeView(EmployeeId);

            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}