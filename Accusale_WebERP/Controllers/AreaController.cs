﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class AreaController : Controller
    {
        AreaSP spArea = new AreaSP();
        // GET: Area
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddArea(AreaInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.AreaId == 0)
            {
                if (spArea.AreaNameCheckExistence(datainfo.AreaName, 0) == false)
                {
                    spArea.AreaAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.AreaId != 0)
            {
                spArea.AreaEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteArea(int AreaId)
        {
            spArea.AreaDelete(AreaId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spArea.AreaViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int AreaId)
        {
            AreaInfo datainfo = spArea.AreaView(AreaId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}