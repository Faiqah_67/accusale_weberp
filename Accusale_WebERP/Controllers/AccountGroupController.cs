﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class AccountGroupController : Controller
    {
        AccountGroupSP spAccountGroup = new AccountGroupSP();
        // GET: AccountGroup
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddData(AccountGroupInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.AccountGroupId == 0)
            {
                if (spAccountGroup.AccountGroupCheckExistence(datainfo.AccountGroupName, 0) == false)
                {
                    spAccountGroup.AccountGroupAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.AccountGroupId != 0)
            {
                spAccountGroup.AccountGroupEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteData(int AccountGroupId)
        {
            spAccountGroup.AccountGroupDelete(AccountGroupId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spAccountGroup.AccountGroupViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int AccountGroupId)
        {
            AccountGroupInfo datainfo = spAccountGroup.AccountGroupView(AccountGroupId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ShowAccountGroup()
        {
            DataTable dt = spAccountGroup.AccountGroupViewAllComboFill();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
    }
}