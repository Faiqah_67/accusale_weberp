
using AccuSale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AccuSale_Final.DAL
{
    class CompanySP 
    {
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void CompanyAdd(CompanyInfo companyinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CompanyAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@companyId", SqlDbType.Decimal);
                sprmparam.Value = companyinfo.CompanyId;
                sprmparam = sccmd.Parameters.Add("@companyName", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.CompanyName;
                sprmparam = sccmd.Parameters.Add("@mailingName", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.MailingName;
                sprmparam = sccmd.Parameters.Add("@address", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Address;
                sprmparam = sccmd.Parameters.Add("@phone", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Phone;
                sprmparam = sccmd.Parameters.Add("@mobile", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Mobile;
                sprmparam = sccmd.Parameters.Add("@emailId", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.EmailId;
                sprmparam = sccmd.Parameters.Add("@web", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Web;
                sprmparam = sccmd.Parameters.Add("@country", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Country;
                sprmparam = sccmd.Parameters.Add("@state", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.State;
                sprmparam = sccmd.Parameters.Add("@pin", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Pin;
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = companyinfo.CurrencyId;
                sprmparam = sccmd.Parameters.Add("@financialYearFrom", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.FinancialYearFrom;
                sprmparam = sccmd.Parameters.Add("@booksBeginingFrom", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.BooksBeginingFrom;
                sprmparam = sccmd.Parameters.Add("@tin", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Tin;
                sprmparam = sccmd.Parameters.Add("@cst", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Cst;
                sprmparam = sccmd.Parameters.Add("@pan", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Pan;
                sprmparam = sccmd.Parameters.Add("@currentDate", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.CurrentDate;
                sprmparam = sccmd.Parameters.Add("@logo", SqlDbType.Image);
                sprmparam.Value = companyinfo.Logo;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.ExtraDate;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in Company Table
        /// </summary>
        /// <param name="companyinfo"></param>
        public void CompanyEdit(CompanyInfo companyinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CompanyEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@companyId", SqlDbType.Decimal);
                sprmparam.Value = companyinfo.CompanyId;
                sprmparam = sccmd.Parameters.Add("@companyName", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.CompanyName;
                sprmparam = sccmd.Parameters.Add("@mailingName", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.MailingName;
                sprmparam = sccmd.Parameters.Add("@address", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Address;
                sprmparam = sccmd.Parameters.Add("@phone", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Phone;
                sprmparam = sccmd.Parameters.Add("@mobile", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Mobile;
                sprmparam = sccmd.Parameters.Add("@emailId", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.EmailId;
                sprmparam = sccmd.Parameters.Add("@web", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Web;
                sprmparam = sccmd.Parameters.Add("@country", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Country;
                sprmparam = sccmd.Parameters.Add("@state", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.State;
                sprmparam = sccmd.Parameters.Add("@pin", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Pin;
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = companyinfo.CurrencyId;
                sprmparam = sccmd.Parameters.Add("@financialYearFrom", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.FinancialYearFrom;
                sprmparam = sccmd.Parameters.Add("@booksBeginingFrom", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.BooksBeginingFrom;
                sprmparam = sccmd.Parameters.Add("@tin", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Tin;
                sprmparam = sccmd.Parameters.Add("@cst", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Cst;
                sprmparam = sccmd.Parameters.Add("@pan", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Pan;
                sprmparam = sccmd.Parameters.Add("@currentDate", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.CurrentDate;
                sprmparam = sccmd.Parameters.Add("@logo", SqlDbType.Image);
                sprmparam.Value = companyinfo.Logo;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from Company Table
        /// </summary>
        /// <returns></returns>
        public DataTable CompanyViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("CompanyViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from Company table based on the parameter
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public CompanyInfo CompanyView(decimal companyId)
        {
            CompanyInfo companyinfo = new CompanyInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CompanyView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@companyId", SqlDbType.Decimal);
                sprmparam.Value = companyId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    companyinfo.CompanyId = decimal.Parse(sdrreader[0].ToString());
                    companyinfo.CompanyName = sdrreader[1].ToString();
                    companyinfo.MailingName = sdrreader[2].ToString();
                    companyinfo.Address = sdrreader[3].ToString();
                    companyinfo.Phone = sdrreader[4].ToString();
                    companyinfo.Mobile = sdrreader[5].ToString();
                    companyinfo.EmailId = sdrreader[6].ToString();
                    companyinfo.Web = sdrreader[7].ToString();
                    companyinfo.Country = sdrreader[8].ToString();
                    companyinfo.State = sdrreader[9].ToString();
                    companyinfo.Pin = sdrreader[10].ToString();
                    companyinfo.CurrencyId = decimal.Parse(sdrreader[11].ToString());
                    companyinfo.FinancialYearFrom = DateTime.Parse(sdrreader[12].ToString());
                    companyinfo.BooksBeginingFrom = DateTime.Parse(sdrreader[13].ToString());
                    companyinfo.Tin = sdrreader[14].ToString();
                    companyinfo.Cst = sdrreader[15].ToString();
                    companyinfo.Pan = sdrreader[16].ToString();
                    companyinfo.CurrentDate = DateTime.Parse(sdrreader[17].ToString());
                    //companyinfo.Logo = (byte[])(sdrreader[18]);
                    companyinfo.Extra1 = sdrreader[19].ToString();
                    companyinfo.Extra2 = sdrreader[20].ToString();
                    companyinfo.ExtraDate = DateTime.Parse(sdrreader[21].ToString());
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return companyinfo;
        }
        
        /// <summary>
        /// Function to get the company details for dotmatrix
        /// </summary>
        /// <returns></returns>
        public DataTable CompanyViewForDotMatrix()
        {
            DataTable dtbl = new DataTable();
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("CompanyViewForDotMatrix", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtbl;
        }
       /// <summary>
       /// Company view details in a datatable
       /// </summary>
       /// <param name="companyId"></param>
       /// <returns></returns>
        public DataTable CompanyViewDataTable(decimal companyId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("CompanyView", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.Add("@companyId", SqlDbType.Decimal).Value = companyId;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="CompanyId"></param>
        public void CompanyDelete(decimal CompanyId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CompanyDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@companyId", SqlDbType.Decimal);
                sprmparam.Value = CompanyId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for Company table
        /// </summary>
        /// <returns></returns>
        public int CompanyGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CompanyMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to insert values to Company Table
        /// </summary>
        /// <param name="companyinfo"></param>
        /// <returns></returns>
        public decimal CompanyAddParticularFeilds(CompanyInfo companyinfo)
        {
            decimal decCopmanyId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CompanyAddParticularFeilds", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@companyName", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.CompanyName;
                sprmparam = sccmd.Parameters.Add("@mailingName", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.MailingName;
                sprmparam = sccmd.Parameters.Add("@address", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Address;
                sprmparam = sccmd.Parameters.Add("@phone", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Phone;
                sprmparam = sccmd.Parameters.Add("@mobile", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Mobile;
                sprmparam = sccmd.Parameters.Add("@emailId", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.EmailId;
                sprmparam = sccmd.Parameters.Add("@web", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Web;
                sprmparam = sccmd.Parameters.Add("@country", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Country;
                sprmparam = sccmd.Parameters.Add("@state", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.State;
                sprmparam = sccmd.Parameters.Add("@pin", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Pin;
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = companyinfo.CurrencyId;
                sprmparam = sccmd.Parameters.Add("@financialYearFrom", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.FinancialYearFrom;
                sprmparam = sccmd.Parameters.Add("@booksBeginingFrom", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.BooksBeginingFrom;
                sprmparam = sccmd.Parameters.Add("@tin", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Tin;
                sprmparam = sccmd.Parameters.Add("@cst", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Cst;
                sprmparam = sccmd.Parameters.Add("@pan", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Pan;
                sprmparam = sccmd.Parameters.Add("@currentDate", SqlDbType.DateTime);
                sprmparam.Value = companyinfo.CurrentDate;
                sprmparam = sccmd.Parameters.Add("@logo", SqlDbType.Image);
                sprmparam.Value = companyinfo.Logo;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = companyinfo.Extra2;
                decCopmanyId = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decCopmanyId;
        }
       /// <summary>
       /// Function to check existance of company
       /// </summary>
       /// <param name="strCompanyName"></param>
       /// <param name="decCompanyId"></param>
       /// <returns></returns>
        public bool CompanyCheckExistence(String strCompanyName, decimal decCompanyId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("CompanyCheckExistence", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@companyName", SqlDbType.VarChar);
                sprmparam.Value = strCompanyName;
                sprmparam = sqlcmd.Parameters.Add("@companyId", SqlDbType.Decimal);
                sprmparam.Value = decCompanyId;
                object obj = sqlcmd.ExecuteScalar();
                decimal decCount = 0;
                if (obj != null)
                {
                    decCount = Convert.ToDecimal(obj.ToString());
                }
                if (decCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return false;
        }
       /// <summary>
       /// Function to count the company
       /// </summary>
       /// <returns></returns>
        public int CompanyCount()
        {
            int inCount = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    try
                    {
                        Sqlcon.Open();
                    }
                    catch
                    {
                        inCount = -1;
                    }
                }
                SqlCommand sccmd = new SqlCommand("CompanyCount", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                inCount = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception)
            {
                inCount = -1;
            }
            finally
            {
                Sqlcon.Close();
            }
            return inCount;
        }
        /// <summary>
        /// Company current date edit
        /// </summary>
        /// <param name="dtCurrentDate"></param>
        public void CompanyCurrentDateEdit(DateTime dtCurrentDate)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("CompanyCurrentDateEdit", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@companyId", SqlDbType.Decimal).Value = 1;//PublicVariables._decCurrentCompanyId;
                sqlcmd.Parameters.Add("@currentDate", SqlDbType.DateTime).Value = dtCurrentDate;
                sqlcmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to view all company for selectcompany form
        /// </summary>
        /// <returns></returns>
        public DataTable CompanyViewAllForSelectCompany()
        {
            DataTable dtbl = new DataTable();
            dtbl.Columns.Add("SlNo", typeof(decimal));
            dtbl.Columns["SlNo"].AutoIncrement = true;
            dtbl.Columns["SlNo"].AutoIncrementSeed = 1;
            dtbl.Columns["SlNo"].AutoIncrementStep = 1;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("CompanyViewAllForSelectCompany", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Company get Id for the company
        /// </summary>
        /// <returns></returns>
        public decimal CompanyGetIdIfSingleCompany()
        {
            decimal decCompanyId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CompanyGetIdIfSingleCompany", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                decCompanyId = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decCompanyId;
        }
        
        /// <summary>
        /// Function for storedprocedure inserter
        /// </summary>
        /// <param name="strParameter"></param>
        /// <returns></returns>
        public string StoredProcedureInserter(string strParameter)
        {
            string error = "";
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                string str = "IF NOT EXISTS(SELECT name FROM sysobjects WHERE type = 'P' AND name='StoredProcedureInserter')"
                + " BEGIN EXECUTE('CREATE PROCEDURE StoredProcedureInserter @parameter varchar(max) AS execute(@parameter)')"
                + " END";
                SqlCommand sqlcmd = new SqlCommand(str, Sqlcon);
                sqlcmd.ExecuteNonQuery();
                SqlCommand sccmd = new SqlCommand("StoredProcedureInserter", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@parameter", SqlDbType.VarChar);
                sprmparam.Value = strParameter;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            finally
            {
                Sqlcon.Close();
            }
            return error;
        }
    
    }
}
