
using Accusale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Accusale_Final.DAL
{
    class BrandSP 
    {
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public decimal BrandAdd(BrandInfo brandinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("BrandAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@brandName", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.BrandName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@manufacturer", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Manufacturer;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = brandinfo.ExtraDate;
                decimal inEffectedRow = Convert.ToDecimal(sccmd.ExecuteScalar());
                if (inEffectedRow > 0)
                {
                    return inEffectedRow;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
                return 0;
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in brand Table
        /// </summary>
        /// <param name="brandinfo"></param>
        /// <returns></returns>
        public bool BrandEdit(BrandInfo brandinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("BrandEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@brandId", SqlDbType.Decimal);
                sprmparam.Value = brandinfo.BrandId;
                sprmparam = sccmd.Parameters.Add("@brandName", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.BrandName;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@manufacturer", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Manufacturer;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = brandinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = brandinfo.ExtraDate;
                int inEffectedRow = sccmd.ExecuteNonQuery();
                if (inEffectedRow > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
                return false;
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from Brand Table
        /// </summary>
        /// <returns></returns>
        public DataTable BrandViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("BrandViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                dtbl.Columns.Add("SLNO", typeof(decimal));
                dtbl.Columns["SLNO"].AutoIncrement = true;
                dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
                dtbl.Columns["SLNO"].AutoIncrementStep = 1;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from brand table based on the parameter
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        public BrandInfo BrandView(decimal brandId)
        {
            BrandInfo brandinfo = new BrandInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("BrandView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@brandId", SqlDbType.Decimal);
                sprmparam.Value = brandId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    brandinfo.BrandId = Convert.ToDecimal(sdrreader[0].ToString());
                    brandinfo.BrandName = sdrreader[1].ToString();
                    brandinfo.Narration = sdrreader[2].ToString();
                    brandinfo.Manufacturer = sdrreader[3].ToString();
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return brandinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="BrandId"></param>
        public void BrandDelete(decimal BrandId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("BrandDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@brandId", SqlDbType.Decimal);
                sprmparam.Value = BrandId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for brand table
        /// </summary>
        /// <returns></returns>
        public int BrandGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("BrandMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to search a brand
        /// </summary>
        /// <param name="strBrandName"></param>
        /// <returns></returns>
        public DataTable BrandSearch(string strBrandName)
        {
         // EmployeeInfo infoEmployee = new EmployeeInfo();
            DataTable dtbl = new DataTable();
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("BrandSearch", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                dtbl.Columns.Add("SLNO", typeof(decimal));
                dtbl.Columns["SLNO"].AutoIncrement = true;
                dtbl.Columns["SLNO"].AutoIncrementSeed = 1;
                dtbl.Columns["SLNO"].AutoIncrementStep = 1;
                sqlda.SelectCommand.Parameters.Add("@brandName", SqlDbType.VarChar).Value = strBrandName;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            return dtbl;
        }
       
        /// <summary>
        /// Function to check existance of a brand
        /// </summary>
        /// <param name="strBrandName"></param>
        /// <param name="decBrandId"></param>
        /// <returns></returns>
        public bool BrandCheckIfExist(string strBrandName, decimal decBrandId)
        {
            bool isEdit = false;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("BrandCheckIfExist", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@brandName", SqlDbType.VarChar);
                sprmparam.Value = strBrandName;
                sprmparam = sccmd.Parameters.Add("@brandId", SqlDbType.Decimal);
                sprmparam.Value = decBrandId;
                object obj = sccmd.ExecuteScalar();
                if (obj != null)
                {
                    if (int.Parse(obj.ToString()) == 0)
                    {
                        isEdit = true;
                    }
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
                return false;
            }
            finally
            {
                Sqlcon.Close();
            }
            return isEdit;
        }
        /// <summary>
        /// Function to delete a brand by checking existance
        /// </summary>
        /// <param name="BrandId"></param>
        /// <returns></returns>
        public decimal BrandDeleteCheckExistence(decimal BrandId)
        {
            decimal decReturnValue = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("BrandDeleteCheckExistence", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@brandId", SqlDbType.Decimal);
                sprmparam.Value = BrandId;
                decReturnValue = Convert.ToDecimal(sccmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decReturnValue;
        }
       
    }
}
