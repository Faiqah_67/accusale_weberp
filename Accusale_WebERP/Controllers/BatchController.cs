﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class BatchController : Controller
    {
        BatchSP spBatch = new BatchSP();

        // GET: Batch
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddBatch(BatchInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.BatchId == 0)
            {
                if (spBatch.BatchNameAndProductNameCheckExistence(datainfo.BatchNo, datainfo.ProductId, 0) == false)
                {
                    spBatch.BatchAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.BatchId != 0)
            {
                spBatch.BatchEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteBatch(int BatchId)
        {
            spBatch.BatchDelete(BatchId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spBatch.BatchViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int BatchId)
        {
            BatchInfo datainfo = new BatchInfo();
            datainfo = spBatch.BatchView(BatchId);
            string ManufacturingDate = datainfo.ManufacturingDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);
            string ExpiryDate = datainfo.ExpiryDate.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture);

            return Json(new { datainfo, ManufacturingDate, ExpiryDate }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ShowProduct()
        {
            ProductSP spProduct = new ProductSP();
            DataTable dt = spProduct.ProductViewAllForComboBox();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
    }
}