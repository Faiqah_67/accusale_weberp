﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class VoucherTypeController : Controller
    {
        VoucherTypeSP spVoucherType = new VoucherTypeSP();
        // GET: VoucherType
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddVoucherType(VoucherTypeInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.VoucherTypeId == 0)
            {
                if (spVoucherType.VoucherTypeCheckExistence(datainfo.VoucherTypeName, 0) == false)
                {
                    spVoucherType.VoucherTypeAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.VoucherTypeId != 0)
            {
                spVoucherType.VoucherTypeEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteVoucherType(int VoucherTypeId)
        {
            spVoucherType.VoucherTypeDelete(VoucherTypeId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = spVoucherType.VoucherTypeViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int VoucherTypeId)
        {

            VoucherTypeInfo datainfo = spVoucherType.VoucherTypeView(VoucherTypeId);

            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult ShowDotMatrixPrintFormat()
        {
            DataTable dt = spVoucherType.DotMatrxPrinterFormatComboFillForVoucherType();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
            
        }
        [HttpGet]
        public ActionResult ShowTypeofVoucher()
        {
            DataTable dt = spVoucherType.TypeOfVoucherComboFill();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
           
        }

    }
}