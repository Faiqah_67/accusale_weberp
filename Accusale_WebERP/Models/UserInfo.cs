﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AccuSale_Final.Models
{
    public class UserInfo
    {
        public decimal UserId { get; set; }

        [Required(ErrorMessage = "Enter Name")]
        public string UserName { get; set; }
        public string Narration { get; set; }
        public decimal RoleId { get; set; }
        public bool Active { get; set; }

        [Required(ErrorMessage = "Enter Password")]
        public string Password { get; set; }
        public string OPassword { get; set; }
        public string NPassword { get; set; }
        public string RPassword { get; set; }

        public DateTime ExtraDate { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
    }
}