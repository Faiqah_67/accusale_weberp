
using System;    

namespace AccuSale_Final.Models 
{    
public class LedgerPostingInfo    
{    
    public decimal _ledgerPostingId;    
    public DateTime _date;    
    public decimal _voucherTypeId;     
    public string _voucherNo;    
    public decimal _ledgerId;    
    public decimal _debit;    
    public decimal _credit;    
     
    public decimal _detailsId;    
    public decimal _yearId;    
    public string _invoiceNo;
    public string _chequeNo;
    public DateTime _chequeDate;
    public DateTime _extraDate;    
    public string _extra1;    
    public string _extra2;  
  
    
    public decimal LedgerPostingId    
    {    
        get { return _ledgerPostingId; }    
        set { _ledgerPostingId = value; }    
    }    
    public DateTime Date    
    {    
        get { return _date; }    
        set { _date = value; }    
    }    
    public decimal VoucherTypeId    
    {
        get { return _voucherTypeId; }
        set { _voucherTypeId = value; }    
    }    
    public string VoucherNo    
    {    
        get { return _voucherNo; }    
        set { _voucherNo = value; }    
    }    
    public decimal LedgerId    
    {    
        get { return _ledgerId; }    
        set { _ledgerId = value; }    
    }    
    public decimal Debit    
    {    
        get { return _debit; }    
        set { _debit = value; }    
    }    
    public decimal Credit    
    {    
        get { return _credit; }    
        set { _credit = value; }    
    }    
  
    public decimal DetailsId    
    {    
        get { return _detailsId; }    
        set { _detailsId = value; }    
    }    
    public decimal YearId    
    {    
        get { return _yearId; }    
        set { _yearId = value; }    
    }    
    public string InvoiceNo   
    {    
        get { return _invoiceNo; }    
        set { _invoiceNo = value; }    
    }
    public string ChequeNo
    {
        get { return _chequeNo; }
        set { _chequeNo = value; }
    }
    public DateTime ChequeDate
    {
        get { return _chequeDate; }
        set { _chequeDate = value; }
    }
    public DateTime ExtraDate    
    {    
        get { return _extraDate; }    
        set { _extraDate = value; }    
    }    
    public string Extra1    
    {    
        get { return _extra1; }    
        set { _extra1 = value; }    
    }    
    public string Extra2    
    {    
        get { return _extra2; }    
        set { _extra2 = value; }    
    }    
    
}    
}
