using System;

namespace AccuSale_Final.Models
{    
public class CurrencyInfo    
{
        public decimal CurrencyId { get; set; }
        public string CurrencySymbol { get; set; }
        public string CurrencyName { get; set; }
        public string SubunitName { get; set; }
        public int NoOfDecimalPlaces { get; set; }
        public string Narration { get; set; }
        public bool IsDefault { get; set; }

        public DateTime ExtraDate=DateTime.Now;

        public string Extra1=string.Empty;

        public string Extra2 = string.Empty;




    }    
}
