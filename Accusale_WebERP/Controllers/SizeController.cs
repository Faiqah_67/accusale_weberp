﻿using AccuSale_Final.DAL;
using AccuSale_Final.Models;
using Newtonsoft.Json;
using System.Data;
using System.Web.Mvc;

namespace Accusale_WebERP.Controllers
{
    public class SizeController : Controller
    {
        SizeSP SizeSP = new SizeSP();

        // GET: Size
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult AddSize(SizeInfo datainfo)
        {
            var outputmsg = string.Empty;


            if (datainfo.SizeId == 0)
            {
                if (SizeSP.SizeNameCheckExistence(datainfo.Size, 0) == false)
                {
                    SizeSP.SizeAdd(datainfo);
                    outputmsg = "Data saved successfully";
                }
                else
                {
                    outputmsg = "Data already exists";

                }
            }


            if (datainfo.SizeId != 0)
            {
                SizeSP.SizeEdit(datainfo); outputmsg = "Data updated successfully";
            }


            return Json(new { message = outputmsg });

        }

        [HttpPost]
        public JsonResult DeleteSize(int SizeId)
        {
            SizeSP.SizeDelete(SizeId);
            return Json(1);
        }

        [HttpGet]
        public JsonResult ShowData()
        {
            DataTable dt = SizeSP.SizeViewAll();
            string json = JsonConvert.SerializeObject(dt);
            return Json(new { json }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditData(int SizeId)
        {
            SizeInfo datainfo = SizeSP.SizeView(SizeId);
            return Json(new { datainfo }, JsonRequestBehavior.AllowGet);
        }
    }
}