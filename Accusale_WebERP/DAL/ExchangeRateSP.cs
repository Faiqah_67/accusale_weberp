
using Accusale_Final.Models;
using System;
using System.Data;
using System.Data.SqlClient;
//<summary>    
//Summary description for ExchangeRateSP    
//</summary>    
namespace AccuSale_Final.DAL
{
    class ExchangeRateSP 
    {
        #region Function
        SqlConnection Sqlcon = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString);

        public void ExchangeRateAdd(ExchangeRateInfo exchangerateinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangeRateAdd", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = exchangerateinfo.CurrencyId;
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = exchangerateinfo.Date;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = exchangerateinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = exchangerateinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in ExchangeRate Table
        /// </summary>
        /// <param name="exchangerateinfo"></param>
        public void ExchangeRateEdit(ExchangeRateInfo exchangerateinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangeRateEdit", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = exchangerateinfo.ExchangeRateId;
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = exchangerateinfo.CurrencyId;
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = exchangerateinfo.Date;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = exchangerateinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from ExchangeRate Table
        /// </summary>
        /// <returns></returns>
        public DataTable ExchangeRateViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("ExchangeRateViewAll", Sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from ExchangeRate Table based on the parameter
        /// </summary>
        /// <param name="exchangeRateId"></param>
        /// <returns></returns>
        public ExchangeRateInfo ExchangeRateView(decimal exchangeRateId)
        {
            ExchangeRateInfo exchangerateinfo = new ExchangeRateInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangeRateView", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = exchangeRateId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    exchangerateinfo.ExchangeRateId = decimal.Parse(sdrreader[0].ToString());
                    exchangerateinfo.CurrencyId = decimal.Parse(sdrreader[1].ToString());
                    exchangerateinfo.Date = DateTime.Parse(sdrreader[2].ToString());
                    exchangerateinfo.Rate = decimal.Parse(sdrreader[3].ToString());
                    exchangerateinfo.Narration = sdrreader[4].ToString();
                    exchangerateinfo.ExtraDate = DateTime.Parse(sdrreader[5].ToString());
                    exchangerateinfo.Extra1 = sdrreader[6].ToString();
                    exchangerateinfo.Extra2 = sdrreader[7].ToString();
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                sdrreader.Close();
                Sqlcon.Close();
            }
            return exchangerateinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="ExchangeRateId"></param>
        public void ExchangeRateDelete(decimal ExchangeRateId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangeRateDelete", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = ExchangeRateId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to  get the next id for ExchangeRate table
        /// </summary>
        /// <returns></returns>
        public int ExchangeRateGetMax()
        {
            int max = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangeRateMax", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to insert particular values to ExchangeRate Table
        /// </summary>
        /// <param name="exchangerateinfo"></param>
        public void ExchangeRateAddParticularFields(ExchangeRateInfo exchangerateinfo)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangeRateAddParticularFields", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = exchangerateinfo.CurrencyId;
                sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = exchangerateinfo.Date;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = exchangerateinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = exchangerateinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get values for search based on parameters
        /// </summary>
        /// <param name="strCurrencyname"></param>
        /// <param name="dtDateFrom"></param>
        /// <param name="dtDateTo"></param>
        /// <returns></returns>
        public DataTable ExchangeRateSearch(String strCurrencyname, DateTime dtDateFrom, DateTime dtDateTo)
        {
            DataTable dtbl = new DataTable();
            dtbl.Columns.Add("SL.NO", typeof(decimal));
            dtbl.Columns["SL.NO"].AutoIncrement = true;
            dtbl.Columns["SL.NO"].AutoIncrementSeed = 1;
            dtbl.Columns["SL.NO"].AutoIncrementStep = 1;
            try
            {
                SqlDataAdapter sqlda = new SqlDataAdapter("ExchangeRateSearch", Sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlda.SelectCommand.Parameters.Add("@currencyName", SqlDbType.VarChar).Value = strCurrencyname;
                sqlda.SelectCommand.Parameters.Add("@dateTo", SqlDbType.DateTime).Value = dtDateTo;
                sqlda.SelectCommand.Parameters.Add("@dateFrom", SqlDbType.DateTime).Value = dtDateFrom;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to check existence based on parameters and return status
        /// </summary>
        /// <param name="dtDate"></param>
        /// <param name="decCurrencyId"></param>
        /// <param name="decExchangeRateId"></param>
        /// <returns></returns>
        public bool ExchangeRateCheckExistence(DateTime dtDate, decimal decCurrencyId, decimal decExchangeRateId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("ExchangeRateCheckExistence", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = dtDate;
                sprmparam = sqlcmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = decExchangeRateId;
                sprmparam = sqlcmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = decCurrencyId;
                object obj = sqlcmd.ExecuteScalar();
                decimal decCount = 0;
                if (obj != null)
                {
                    decCount = Convert.ToDecimal(obj.ToString());
                }
                if (decCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return false;
        }
        /// <summary>
        /// Function to get id based on parameter
        /// </summary>
        /// <param name="decCurrencyId"></param>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public decimal GetExchangeRateId(decimal decCurrencyId, DateTime dtDate)
        {
            decimal decCount = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("GetExchangeRateId", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@currencyId", SqlDbType.Decimal).Value = decCurrencyId;
                sqlcmd.Parameters.Add("@date", SqlDbType.DateTime).Value = dtDate;
                Object obj = sqlcmd.ExecuteScalar();
                if (obj != null)
                {
                    decCount = Convert.ToDecimal(obj.ToString());
                }
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decCount;
        }
        /// <summary>
        /// Function to check refernce bassed on parameter
        /// </summary>
        /// <param name="decExchangeRateId"></param>
        /// <returns></returns>
        public decimal ExchangeRateCheckReferences(decimal decExchangeRateId)
        {
            decimal decReturnValue = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("ExchangeRateCheckReferences", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = decExchangeRateId;
                decReturnValue = Convert.ToDecimal(sqlcmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decReturnValue;
        }
        /// <summary>
        /// Function to get rate based on parameter
        /// </summary>
        /// <param name="decExchangeRateId"></param>
        /// <returns></returns>
        public decimal GetExchangeRateByExchangeRateId(decimal decExchangeRateId)
        {
            decimal decReturnValue = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("GetExchangeRateByExchangeRateId", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = decExchangeRateId;
                decReturnValue = Convert.ToDecimal(sqlcmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decReturnValue;
        }
        /// <summary>
        /// Function to view rate based on parameter
        /// </summary>
        /// <param name="decExchangeRateId"></param>
        /// <returns></returns>
        public decimal ExchangeRateViewByExchangeRateId(decimal decExchangeRateId)
        {
            decimal exchangeRate = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangeRateViewByExchangeRateId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = decExchangeRateId;
                exchangeRate = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return exchangeRate;
        }
        /// <summary>
        /// Function to get decimal places based on parameter
        /// </summary>
        /// <param name="decExchangeRateId"></param>
        /// <returns></returns>
        public int NoOfDecimalNumberViewByExchangeRateId(decimal decExchangeRateId)
        {
            int NoOfDecimalNumber = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("NoOfDecimalNumberViewByExchangeRateId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = decExchangeRateId;
                NoOfDecimalNumber = Convert.ToInt32(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return NoOfDecimalNumber;
        }
        /// <summary>
        /// Function to get decimal places based on parameter
        /// </summary>
        /// <param name="decCurrencyId"></param>
        /// <returns></returns>
        public int NoOfDecimalNumberViewByCurrencyId(decimal decCurrencyId)
        {
            int NoOfDecimalNumber = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("NoOfDecimalNumberViewByCurrencyId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = decCurrencyId;
                NoOfDecimalNumber = Convert.ToInt32(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return NoOfDecimalNumber;
        }
        /// <summary>
        /// Function to get value based on parameter
        /// </summary>
        /// <param name="decCurrencyId"></param>
        /// <returns></returns>
        public decimal ExchangerateViewByCurrencyId(decimal decCurrencyId)
        {
            decimal decExchangerateId = 0;
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("ExchangerateViewByCurrencyId", Sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@currencyId", SqlDbType.Decimal);
                sprmparam.Value = decCurrencyId;
                decExchangerateId = Convert.ToInt32(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
               ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return decExchangerateId;
        }


        /// <summary>
        /// Function to check existence based on parameters and return status
        /// </summary>
        /// <param name="dtDate"></param>
        /// <param name="decCurrencyId"></param>
        /// <param name="decExchangeRateId"></param>
        /// <returns></returns>
        public bool ExchangeRateCheckExistanceForUpdationAndDelete(DateTime dtDate, decimal decExchangeRateId)
        {
            try
            {
                if (Sqlcon.State == ConnectionState.Closed)
                {
                    Sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("ExchangeRateCheckExistanceForUpdationAndDelete", Sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@date", SqlDbType.DateTime);
                sprmparam.Value = dtDate;
                sprmparam = sqlcmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = decExchangeRateId;
                object obj = sqlcmd.ExecuteScalar();
                decimal decCount = 0;
                if (obj != null)
                {
                    decCount = Convert.ToDecimal(obj.ToString());
                }
                if (decCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                Sqlcon.Close();
            }
            return false;
        }
        #endregion
    }
}
